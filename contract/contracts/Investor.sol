// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
pragma experimental ABIEncoderV2;

import "./ITransactable.sol";
import "./BaseCurrency.sol";
import "./Broker.sol";

contract Investor is ITransactable {
    constructor(
        uint256 _id,
        uint256 _taxId,
        string memory _fName,
        string memory _lName
    ) {
        ID = _id;
        taxID = _taxId;
        firstName = _fName;
        lastName = _lName;
        isRegistered = true;
    }

    address private addressBaseCurrency;
    BaseCurrency private BSC;
    uint256 private ID;
    uint256 private taxID;
    string private firstName;
    string private lastName;
    mapping(string => Holding) private portfolio;
    mapping(string => ShortOrder[]) private activeShorts;
    string[] private ownedTickers;
    bool private isRegistered;
    string private registeredAt;
    uint256 private USD_TO_WEI = 240000000000000;

    /** @dev external orderType implementation */
    uint256 private orderType_LONG = 0;
    uint256 private orderType_SELL = 1;
    uint256 private orderType_SHORT = 2;

    struct Holding {
        uint256 totalPricesofBuys;
        uint256 buysCounter;
        uint256 totalSharesOwned;
        bool isValue;
    }

    struct Stock {
        string tickerSymbol;
        uint256 decimals;
        uint256 price;
        bool isValue;
    }

    struct ShortOrder {
        uint256 orderId;
        string tickerSymbol;
        uint256 price;
        uint256 shareAmount;
        bool isValue;
    }

    event BSCMinted(address _target, uint256 _amount);
    event BalanceUpdated(uint256 _oldBalance, uint256 _newBalance);
    event Received(address _addr, uint256 _amount);
    event FaultedOnHolding(string _ticker);

    event NewShortOrder(
        string _ticker,
        uint256 _shareAmount,
        uint256 _price,
        uint256 _matched
    );

    event OutgoingPayment(uint256 bid, uint256 totalPrice, address receiver);

    modifier mustBeCovered(
        uint256 _amount,
        address _addr,
        address _bscAddress
    ) {
        BSC = BaseCurrency(payable(_bscAddress));
        uint256 bscBalance = BSC.balanceOf(_addr);
        require(bscBalance >= _amount, "Not enough funds");
        _;
    }

    modifier mustBeCoveredWithWei(uint256 _amount) {
        require(
            msg.value >= _amount * USD_TO_WEI,
            "You need to cover the minting process with enough Wei. 1 USD = 1 BSC"
        );
        _;
    }

    fallback() external payable {
        // fallback to receive and store ETH
    }

    receive() external payable {
        // fallback to receive and store ETH
        emit Received(msg.sender, msg.value);
    }

    function _purify(uint256 index) internal {
        require(index < ownedTickers.length);
        ownedTickers[index] = ownedTickers[ownedTickers.length - 1];
        delete ownedTickers[ownedTickers.length - 1];
        ownedTickers.pop();
    }

    function _deleteShortOrder(string memory _ticker, uint256 _index) internal {
        require(_index < activeShorts[_ticker].length);
        if (activeShorts[_ticker].length == 1) {
            activeShorts[_ticker].pop();
        } else {
            while (
                !activeShorts[_ticker][activeShorts[_ticker].length - 1].isValue
            ) {
                activeShorts[_ticker].pop();
            }
            activeShorts[_ticker][_index] = activeShorts[_ticker][
                activeShorts[_ticker].length - 1
            ];
            delete activeShorts[_ticker][activeShorts[_ticker].length - 1];
            activeShorts[_ticker].pop();
        }
    }

    function hasRegistered() public view returns (bool) {
        return isRegistered;
    }

    function getOwnedTickers() public view returns (string[] memory) {
        return ownedTickers;
    }

    function getShortsForTicker(string memory _ticker)
        public
        view
        returns (ShortOrder[] memory)
    {
        return activeShorts[_ticker];
    }

    function getBroker() external view returns (string memory) {
        if (isRegistered) {
            return registeredAt;
        } else {
            return "";
        }
    }

    function setBroker(string memory brokerName) public {
        registeredAt = brokerName;
        isRegistered = true;
    }

    function enlistAtBroker(address _brokerAddr, string memory _brokerName)
        external
    {
        Broker broker = Broker(_brokerAddr);
        broker.addShareholder(address(this));
        setBroker(_brokerName);
    }

    /** @dev simple holding deduction */
    function _deductFromHoldings(string memory _ticker, uint256 _shareAmount)
        internal
    {
        uint256 ownedAmount = portfolio[_ticker].totalSharesOwned;
        // require(_shareAmount <= ownedAmount, "Not enough shares!");
        // uint256 newAmount = ownedAmount - _shareAmount;
        if (ownedAmount <= 0) {
            // delete portfolio[_ticker];
            // emit FaultedOnHolding(_ticker);
        } else {
            portfolio[_ticker].totalSharesOwned -= _shareAmount;
        }
    }

    /** @dev simple function to add to holdings  */
    function _addToHoldings(string memory _ticker, uint256 _shareAmount)
        internal
    {
        require(_shareAmount > 0, "U can't add 0 shares");
        if (portfolio[_ticker].totalSharesOwned > 0) {
            portfolio[_ticker].totalSharesOwned += _shareAmount;
        } else {
            ownedTickers.push(_ticker);
            portfolio[_ticker].totalSharesOwned = _shareAmount;
        }
    }

    function handleAccountBalances(Broker.Order memory _order) external {
        if (address(_order.shareholder) == address(this) && _order.isFilled) {
            if (_order.typeOfOrder == orderType_LONG) {
                _handleAccountBalancesForLong(
                    _order.tickerSymbol,
                    _order.price,
                    _order.amountOfShares
                );
            }
            if (_order.typeOfOrder == orderType_SELL) {
                _handleAccountBalancesForSell(
                    _order.tickerSymbol,
                    _order.amountOfShares
                );
            }
            if (_order.typeOfOrder == orderType_SHORT) {
                _handleAccountBalancesForShort(
                    _order.orderId,
                    _order.tickerSymbol,
                    _order.price,
                    _order.amountOfShares
                );
            }
        }
    }

    /** @dev updates holdings and portfolio after a new long position. Note: is not handling payments */
    function _handleAccountBalancesForLong(
        string memory _ticker,
        uint256 _bid,
        uint256 _shareAmount
    ) internal {
        if (!portfolio[_ticker].isValue) {
            ownedTickers.push(_ticker);
            Holding memory newHolding =
                Holding({
                    totalPricesofBuys: _bid,
                    buysCounter: 1,
                    totalSharesOwned: _shareAmount,
                    isValue: true
                });
            portfolio[_ticker] = newHolding;
        } else {
            Holding storage holding = portfolio[_ticker];
            holding.totalPricesofBuys += _bid;
            holding.buysCounter++;
            _addToHoldings(_ticker, _shareAmount);
        }
    }

    /** @dev updates holdings after a sell */
    function _handleAccountBalancesForSell(
        string memory _ticker,
        uint256 _shareAmount
    ) internal {
        _deductFromHoldings(_ticker, _shareAmount);
    }

    /** @dev updates internal shortOrders mapping on new short position initialization */
    function _handleAccountBalancesForShort(
        uint256 _orderId,
        string memory _ticker,
        uint256 _bid,
        uint256 _shareAmount
    ) internal {
        ownedTickers.push(_ticker);
        ShortOrder memory newShort =
            ShortOrder({
                orderId: _orderId,
                tickerSymbol: _ticker,
                price: _bid,
                shareAmount: _shareAmount,
                isValue: true
            });
        activeShorts[_ticker].push(newShort);
    }

    /** @dev exchange Wei for BSC, mint it to the user. */
    function obtainBSC(address _bscAddress, uint256 _amount)
        public
        payable
        mustBeCoveredWithWei(_amount)
        returns (bool)
    {
        BSC = BaseCurrency(payable(_bscAddress));
        if (BSC.mintBSCToTrader(_amount)) {
            emit BSCMinted(payable(address(this)), _amount);
            return true;
        } else {
            return false;
        }
    }

    /** @dev external business logic: implementation of LONG order initialization from the Investor's prespective.
     * Short pseudocode:
     * 1. Add LONG order to broker
     * 2. Pay bought shares out
     * 3. Handle account balances for investor
     */
    function long(
        string memory _ticker,
        uint256 _requestedShares,
        uint256 _bid,
        address _brokerAddress,
        address _bscAddress
    ) public payable override {
        Broker broker = Broker(_brokerAddress);
        Broker.Order[] memory matchedOrders =
            broker.addOrderForShareholder(
                address(this),
                orderType_LONG,
                _ticker,
                _bid,
                _requestedShares
            );
        uint256 noOfOrders = matchedOrders.length;
        for (uint8 i = 0; i < noOfOrders; i++) {
            address addressOfOrder = matchedOrders[i].shareholder;
            if (addressOfOrder != address(this)) {
                //pay the shares.
                uint256 shareAmount = matchedOrders[i].amountOfShares;
                uint256 totalPrice = (shareAmount * _bid) / 100;
                emit OutgoingPayment(
                    _bid,
                    totalPrice,
                    matchedOrders[i].shareholder
                );
                BSC = BaseCurrency(payable(_bscAddress));
                BSC.transferBSC(
                    payable(address(this)),
                    payable(matchedOrders[i].shareholder),
                    totalPrice
                );
            }
        }
    }

    /** @dev external business logic: implementation of SELL order initialization from the Investor's prespective.
     * Short pseudocode:
     * 1. Add SELL order to broker
     * 2. Handle account balances for investor
     */
    function sell(
        string memory _ticker,
        uint256 _givenShares,
        uint256 _ask,
        address _brokerAddress
    ) public payable override {
        Broker broker = Broker(_brokerAddress);
        Broker.Order[] memory matchedOrders =
            broker.addOrderForShareholder(
                address(this),
                orderType_SELL,
                _ticker,
                _ask,
                _givenShares
            );
        uint256 noOfOrders = matchedOrders.length;
        for (uint8 i = 0; i < noOfOrders; i++) {
            address addressOfOrder = matchedOrders[i].shareholder;
            if (addressOfOrder == address(this)) {
                _handleAccountBalancesForSell(
                    _ticker,
                    matchedOrders[i].amountOfShares
                );
            }
        }
    }

    /** @dev external business logic: implementation of SHORT order initialization from the Investor's prespective.
     * Short pseudocode:
     * 1. Add SHORT order to broker
     * 2. Handle account balances for investor updateing activeShorts
     * Note: closing short orders will be done from another endpoint.
     */
    function short(
        string memory _ticker,
        uint256 _requestedShares,
        uint256 _ask,
        address _brokerAddress
    ) external override {
        Broker broker = Broker(_brokerAddress);
        Broker.Order[] memory matchedOrders =
            broker.addOrderForShareholder(
                address(this),
                orderType_SHORT,
                _ticker,
                _ask,
                _requestedShares
            );
        uint256 noOfOrders = matchedOrders.length;
        uint256 valueOfShortsMatched = 0;
        for (uint8 i = 0; i < noOfOrders; i++) {
            _handleAccountBalancesForShort(
                matchedOrders[i].orderId,
                matchedOrders[i].tickerSymbol,
                matchedOrders[i].price,
                matchedOrders[i].amountOfShares
            );
            uint256 orderValue =
                (matchedOrders[i].amountOfShares * matchedOrders[i].price) /
                    100;
            valueOfShortsMatched += orderValue;
            valueOfShortsMatched -= (_ask * _requestedShares) / 100;
        }
        emit NewShortOrder(
            _ticker,
            _requestedShares,
            _ask,
            valueOfShortsMatched
        );
    }

    function closeShortPosition(
        string memory _ticker,
        uint256 _orderId,
        address _brokerAddress,
        address _bscAddress
    ) external payable override {
        Broker broker = Broker(address(_brokerAddress));
        BSC = BaseCurrency(payable(_bscAddress));
        for (uint256 i = 0; i < activeShorts[_ticker].length; i++) {
            ShortOrder memory shortOrder = activeShorts[_ticker][i];
            if (shortOrder.orderId == _orderId) {
                int256 priceDifference =
                    int256(
                        int256(
                            broker.getLowestAskForTicker(
                                shortOrder.tickerSymbol
                            )
                        ) - int256(shortOrder.price)
                    );
                int256 value =
                    (priceDifference * int256(shortOrder.shareAmount)) / 100;
                if (value < 0) {
                    BSC.mintBSCToTrader(uint256(value * -1));
                } else {
                    if (value > 0) {
                        BSC.transferBSC(
                            address(this),
                            _brokerAddress,
                            uint256(value * -1)
                        );
                    }
                }
            }
            _deleteShortOrder(_ticker, i);
        }
    }
}
