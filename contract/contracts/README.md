# Dissertation-2021

## Solidity code structure

### File structure

All Solidity contracts follow the next organizational structure:

1. Constructor
2. State variables
3. Enums
4. Structs
5. Events
6. Modifiers
7. Fallback and receive functions(in case it's needed)
8. Getters and setters
9. Internal utility functions
10. External business logic

### Realm of abstraction

#### BaseCurrency

BaseCurrency.sol holds the basic value transaction mechanism and Stock data.

#### Investor

Investor.sol holds the interaction mechanism of a shareholder to the system.
SecondInvestor.sol is a contract created for testing purposes.

#### Broker

Broker.sol holds the basic order management and market matching mechanisms.
MarketMaker.sol and DTCC.sol are derived from Broker.sol and have a superset of the broker's functions.

#### Market Maker

MarketMaker.sol is just an abstraction of what a real world market maker does as an external liquidity provider to the ecosystem. In the minimal setup, is interpreted as a different subset of an investor, which is used to provide instant macthing orders.

#### SEC

SEC.sol is, like MarketMaker.sol, an abstraction of the logic that the SEC does in the real world. It is responsible for gathering the order logs, analysing suspicious market activities and taking action in case. Since it is out of scope of our project, in our minimal setup simple boolean state variables will determine whether any action takign is necessary.
