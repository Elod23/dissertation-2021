// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
pragma abicoder v2;

import "./Broker.sol";
import "./BaseCurrency.sol";

contract SEC {
    address[] private brokers;
    mapping(address => Broker.Order[]) private ordersOfLastDay;
    bool private suspiciousActivity;
    bool private isRestrictingStock;
    address private bscAddress;
    address private selfAddress;

    modifier canBeCalledOnlyBySelf() {
        require(
            address(this) == selfAddress,
            "Only the SEC contract can call this method!"
        );
        _;
    }

    modifier newBroker(address _brokerAddress) {
        bool isNew = true;
        for (uint256 i = 0; i < brokers.length; i++) {
            if (brokers[i] == _brokerAddress) {
                isNew = false;
                break;
            }
        }
        require(isNew, "Broker is already enlisted!");
        _;
    }

    function setBscAddress(address _bscAddress) public canBeCalledOnlyBySelf() {
        bscAddress = _bscAddress;
    }

    function enableSuspiciousActivity() public canBeCalledOnlyBySelf() {
        suspiciousActivity = true;
    }

    function enableStockRestrictions() public canBeCalledOnlyBySelf() {
        isRestrictingStock = true;
    }

    function setSelfAddressForTests(address _selfAddress) public {
        selfAddress = _selfAddress;
    }

    function getSelfAddress() public view returns (address) {
        return selfAddress;
    }

    function getStockDetailsForTicker(string memory _ticker)
        public
        view
        canBeCalledOnlyBySelf()
        returns (BaseCurrency.Stock memory)
    {
        BaseCurrency BSC = BaseCurrency(payable(bscAddress));
        return BSC.getStockDetailsForTicker(_ticker);
    }

    function getOrdersOfLastDay(address _brokerAddress)
        public
        view
        returns (Broker.Order[] memory)
    {
        return ordersOfLastDay[_brokerAddress];
    }

    function enlistBroker(address _brokerAddress)
        public
        canBeCalledOnlyBySelf()
        newBroker(_brokerAddress)
    {
        Broker broker = Broker(_brokerAddress);
        broker.setSEC(selfAddress);
        brokers.push(_brokerAddress);
    }

    function analyseOrdersForTicker(
        string memory _ticker,
        address _brokerAddress
    ) public canBeCalledOnlyBySelf() {
        BaseCurrency BSC = BaseCurrency(payable(bscAddress));
        BaseCurrency.Stock memory stockDetails =
            BSC.getStockDetailsForTicker(_ticker);
        Broker broker = Broker(_brokerAddress);
        broker.provideOrdersOfLastDay();
        if (suspiciousActivity) {
            // suspend shorting
            stockDetails.borrowable = 0;
            broker.prohibitShorting();
        }
        if (isRestrictingStock) {
            stockDetails.additionalOfferingsAllowed = false;
        }
        BSC.updateStockDetails(stockDetails);
    }
}
