// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
pragma experimental ABIEncoderV2;

import "./Investor.sol";

contract SecondInvestor is Investor {
    constructor(
        uint256 _id,
        uint256 _taxId,
        string memory _fName,
        string memory _lName
    ) Investor(_id, _taxId, _fName, _lName) {}
}
