// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
pragma experimental ABIEncoderV2;

import "./Investor.sol";

contract Broker {
    constructor(string memory _name) {
        name = _name;
    }

    string private name;

    uint256 private order_ids;

    address private secAddress;

    /** @dev list of shareholders and the book to check them */
    address[] private shareholders;

    mapping(address => bool) private shareholderBook;

    /** @dev mapping of orderBook for every shareholder */
    mapping(address => Order[]) private orderBook;

    /** @dev mapping that contains the bids linked to the ticker & price */
    mapping(string => mapping(uint256 => Order[])) private bids;
    /** @dev mapping containing the asks linked to ticker & price */
    mapping(string => mapping(uint256 => Order[])) private asks;

    /** @dev list of tickers known by this broker */
    string[] private tickers;

    bool shortingProhibited;

    /** @dev market balance of share supply, demand and borrowed share count for this broker */
    mapping(string => MarketBalance) private marketBalances;

    /** @dev helps to compute filled orders. Get cleared after every order matching */
    Order[] private filledOrders;
    /** @dev helps to compute total orders of the day */
    Order[] private totalOrders;

    uint256 private OrderType_LONG = 0;
    uint256 private OrderType_SELL = 1;
    uint256 private OrderType_SHORT = 2;

    struct Order {
        uint256 orderId;
        uint256 time;
        uint256 typeOfOrder;
        address shareholder;
        string tickerSymbol;
        uint256 amountOfShares;
        uint256 price;
        bool isValue;
        bool isFilled;
    }

    struct MarketBalance {
        uint256 supply;
        uint256 demand;
        uint256 borrowed;
        uint256 maxBid;
        uint256 minAsk;
        bool isValue;
    }

    event NewOrderForShareholder(address _addr, Order _order);

    event NewShareholder(address _addr);

    event MarketBalanceUpdated(
        string _ticker,
        uint256 _minAsk,
        uint256 _maxBid
    );

    event OrderFilled(
        uint256 orderId,
        address shareholderAddress,
        string tickerSymbol,
        uint256 amountOfSharesFilled
    );

    event OrderPartiallyFilled(
        uint256 orderId,
        address shareholderAddress,
        string tickerSymbol,
        uint256 amountOfSharesFilled
    );

    event OrderReduced(
        uint256 orderId,
        address shareholderAddress,
        string tickerSymbol,
        uint256 amountOfSharesRemaining
    );

    event NewStockListingOnBroker(
        address addr,
        string tickerSymbol,
        uint256 price,
        uint256 amountOfShares,
        string name
    );

    event NumberOfOrdersFilled(uint256 number);

    event ShortingProhibited();

    modifier canBeCalledOnlyBySEC() {
        require(msg.sender == secAddress, "Callign this method is prohibited!");
        _;
    }

    modifier mustBeShareholder(address _addr) {
        require(shareholderBook[_addr] == true, "There is no such shareholder");
        _;
    }

    modifier mustKnowTicker(string memory _ticker) {
        require(
            marketBalances[_ticker].isValue,
            "This broker has no market balances for this ticker"
        );
        _;
    }

    function getName() public view virtual returns (string memory brokerName) {
        return name;
    }

    function getTickers()
        public
        view
        virtual
        returns (string[] memory listedTickers)
    {
        return tickers;
    }

    function getShareholders()
        public
        view
        virtual
        returns (address[] memory shareholdersListed)
    {
        return shareholders;
    }

    function getOrdersForShareholder(address _shareholderAddr)
        public
        view
        mustBeShareholder(_shareholderAddr)
        returns (Order[] memory orderBookForShareholder)
    {
        return orderBook[_shareholderAddr];
    }

    function getMarketBalanceForSecurity(string memory _ticker)
        public
        view
        mustKnowTicker(_ticker)
        returns (
            uint256 supply,
            uint256 demand,
            uint256 borrowed,
            uint256 highestBid,
            uint256 lowestAsk
        )
    {
        return (
            marketBalances[_ticker].supply,
            marketBalances[_ticker].demand,
            marketBalances[_ticker].borrowed,
            marketBalances[_ticker].maxBid,
            marketBalances[_ticker].minAsk
        );
    }

    function getLowestAskForTicker(string memory _ticker)
        external
        view
        returns (uint256)
    {
        uint256 supply;
        uint256 demand;
        uint256 borrowed;
        uint256 highestBid;
        uint256 lowestAsk;
        (
            supply,
            demand,
            borrowed,
            highestBid,
            lowestAsk
        ) = getMarketBalanceForSecurity(_ticker);
        return lowestAsk;
    }

    function getPriceDifferenceForTicker(string memory _ticker)
        external
        view
        returns (int256)
    {
        uint256 supply;
        uint256 demand;
        uint256 borrowed;
        uint256 highestBid;
        uint256 lowestAsk;
        (
            supply,
            demand,
            borrowed,
            highestBid,
            lowestAsk
        ) = getMarketBalanceForSecurity(_ticker);
        return int256(highestBid - lowestAsk);
    }

    function setSEC(address _secAddress) public {
        secAddress = _secAddress;
    }

    function getSEC() public view returns (address) {
        return secAddress;
    }

    function prohibitShorting() public canBeCalledOnlyBySEC() {
        shortingProhibited = true;
        emit ShortingProhibited();
    }

    function addShareholder(address _addr) public {
        if (!shareholderBook[_addr] == true) {
            shareholders.push(_addr);
            shareholderBook[_addr] = true;
            emit NewShareholder(_addr);
        }
    }

    function addSupplyAfterListing(
        string memory _ticker,
        uint256 _price,
        uint256 _noOfShares,
        address _addr
    ) public {
        Order memory order =
            Order({
                orderId: order_ids++,
                time: block.timestamp,
                typeOfOrder: OrderType_SELL,
                shareholder: _addr,
                tickerSymbol: _ticker,
                price: _price,
                amountOfShares: _noOfShares,
                isValue: true,
                isFilled: false
            });
        orderBook[_addr].push(order);
        asks[order.tickerSymbol][order.price].push(order);
        _matchMarketBalanceToOrder(order);
        emit NewStockListingOnBroker(
            _addr,
            order.tickerSymbol,
            order.price,
            order.amountOfShares,
            name
        );
    }

    function _cloneOrder(Order memory _order)
        internal
        pure
        returns (Order memory)
    {
        Order memory newOrder =
            Order({
                orderId: _order.orderId,
                time: _order.time,
                typeOfOrder: _order.typeOfOrder,
                shareholder: _order.shareholder,
                tickerSymbol: _order.tickerSymbol,
                amountOfShares: _order.amountOfShares,
                price: _order.price,
                isValue: _order.isValue,
                isFilled: _order.isFilled
            });
        return newOrder;
    }

    /** @dev  summs up the market balance for security tickerSymbol,
     starts order matching, in strict chronological order. */
    function _matchMarketBalanceToOrder(Order memory _order)
        private
        returns (Order[] storage)
    {
        _checkIfFillable(_order);
        if (!_order.isFilled) {
            uint256 _orderType = 0;
            if (!marketBalances[_order.tickerSymbol].isValue) {
                MarketBalance memory marketBalanceOfTicker =
                    MarketBalance({
                        supply: 0,
                        demand: 0,
                        borrowed: 0,
                        maxBid: 0,
                        minAsk: 2**256 - 1,
                        isValue: true
                    });
                marketBalances[_order.tickerSymbol] = marketBalanceOfTicker;
                tickers.push(_order.tickerSymbol);
            }
            if (_order.typeOfOrder == OrderType_LONG) {
                marketBalances[_order.tickerSymbol].demand += _order
                    .amountOfShares;
                if (marketBalances[_order.tickerSymbol].maxBid < _order.price) {
                    marketBalances[_order.tickerSymbol].maxBid = _order.price;
                }
                bids[_order.tickerSymbol][_order.price].push(_order);
            } else if (_order.typeOfOrder == OrderType_SELL) {
                _orderType = 1;
                if (marketBalances[_order.tickerSymbol].minAsk > _order.price) {
                    marketBalances[_order.tickerSymbol].minAsk = _order.price;
                }
                marketBalances[_order.tickerSymbol].supply += _order
                    .amountOfShares;
                asks[_order.tickerSymbol][_order.price].push(_order);
            } else {
                _orderType = 2;
                marketBalances[_order.tickerSymbol].borrowed += _order
                    .amountOfShares;
            }
            emit MarketBalanceUpdated(
                _order.tickerSymbol,
                marketBalances[_order.tickerSymbol].minAsk,
                marketBalances[_order.tickerSymbol].maxBid
            );
        }
        return filledOrders;
    }

    /** @dev fills the orders outright if possible based on the current market balances */
    function _checkIfFillable(Order memory _order) private {
        if (_order.typeOfOrder == OrderType_LONG) {
            if (
                (marketBalances[_order.tickerSymbol].minAsk <= _order.price) &&
                (marketBalances[_order.tickerSymbol].supply > 0)
            ) {
                uint256 clearedUntil = 0;
                uint256 pos = 0;
                uint256 orderShares = _order.amountOfShares;
                Order[] storage asksWithMatchingPrice =
                    asks[_order.tickerSymbol][_order.price];
                while (
                    (clearedUntil < orderShares) &&
                    (pos < asksWithMatchingPrice.length)
                ) {
                    Order storage counterPart = asksWithMatchingPrice[pos];
                    if (
                        !counterPart.isFilled &&
                        counterPart.amountOfShares > clearedUntil
                    ) {
                        clearedUntil = orderShares;
                        counterPart.amountOfShares -= clearedUntil;

                        emit OrderPartiallyFilled(
                            counterPart.orderId,
                            counterPart.shareholder,
                            counterPart.tickerSymbol,
                            clearedUntil
                        );
                        Order memory partialOrder = _cloneOrder(counterPart);
                        partialOrder.amountOfShares = _order.amountOfShares;
                        partialOrder.isFilled = true;
                        filledOrders.push(partialOrder);
                        emit OrderReduced(
                            counterPart.orderId,
                            counterPart.shareholder,
                            counterPart.tickerSymbol,
                            counterPart.amountOfShares
                        );
                    } else if (!counterPart.isFilled) {
                        clearedUntil += counterPart.amountOfShares;
                        counterPart.isFilled = true;
                        emit OrderFilled(
                            counterPart.orderId,
                            counterPart.shareholder,
                            counterPart.tickerSymbol,
                            counterPart.amountOfShares
                        );
                        filledOrders.push(counterPart);
                    }
                    pos++;
                }
                marketBalances[_order.tickerSymbol].supply -= clearedUntil;
                if (clearedUntil == _order.amountOfShares) {
                    _order.isFilled = true;
                    emit OrderFilled(
                        _order.orderId,
                        _order.shareholder,
                        _order.tickerSymbol,
                        _order.amountOfShares
                    );
                    filledOrders.push(_order);
                } else {
                    _order.amountOfShares -= clearedUntil;
                    if (clearedUntil > 0) {
                        emit OrderPartiallyFilled(
                            _order.orderId,
                            _order.shareholder,
                            _order.tickerSymbol,
                            clearedUntil
                        );
                        emit OrderReduced(
                            _order.orderId,
                            _order.shareholder,
                            _order.tickerSymbol,
                            _order.amountOfShares
                        );
                    }
                }
            }
        } else if (_order.typeOfOrder == OrderType_SELL) {
            if (
                (marketBalances[_order.tickerSymbol].maxBid >= _order.price) &&
                (marketBalances[_order.tickerSymbol].demand > 0)
            ) {
                uint256 clearedUntil = 0;
                uint256 pos = 0;
                uint256 newOrderShares = _order.amountOfShares;
                Order[] storage bidsWithMatchingPrice =
                    bids[_order.tickerSymbol][_order.price];
                while (
                    clearedUntil < newOrderShares &&
                    pos < bidsWithMatchingPrice.length
                ) {
                    Order storage counterBid = bidsWithMatchingPrice[pos];
                    if (
                        !counterBid.isFilled &&
                        counterBid.amountOfShares > newOrderShares
                    ) {
                        clearedUntil = newOrderShares;
                        counterBid.amountOfShares -= (newOrderShares -
                            clearedUntil);
                        emit OrderPartiallyFilled(
                            counterBid.orderId,
                            counterBid.shareholder,
                            counterBid.tickerSymbol,
                            _order.amountOfShares
                        );
                        emit OrderReduced(
                            counterBid.orderId,
                            counterBid.shareholder,
                            counterBid.tickerSymbol,
                            counterBid.amountOfShares
                        );
                    } else if (!counterBid.isFilled) {
                        clearedUntil += counterBid.amountOfShares;
                        counterBid.isFilled = true;
                        emit OrderFilled(
                            counterBid.orderId,
                            counterBid.shareholder,
                            counterBid.tickerSymbol,
                            counterBid.amountOfShares
                        );
                        filledOrders.push(counterBid);
                    }
                    pos++;
                }
                marketBalances[_order.tickerSymbol].demand -= clearedUntil;
                if (clearedUntil == newOrderShares) {
                    _order.isFilled = true;
                    emit OrderFilled(
                        _order.orderId,
                        _order.shareholder,
                        _order.tickerSymbol,
                        _order.amountOfShares
                    );
                    filledOrders.push(_order);
                } else {
                    _order.amountOfShares -= clearedUntil;
                    emit OrderPartiallyFilled(
                        _order.orderId,
                        _order.shareholder,
                        _order.tickerSymbol,
                        clearedUntil
                    );
                    emit OrderReduced(
                        _order.orderId,
                        _order.shareholder,
                        _order.tickerSymbol,
                        _order.amountOfShares
                    );
                }
            }
        } else {
            // we need to have available stocks somewhere  on the market.
            // Since we deduct supply for every filled order, we can leverage
            // supply + demand. This might overchurn a bit, but is a great indicator
            // nonetheless.bytes
            uint256 borrowable =
                (marketBalances[_order.tickerSymbol].supply +
                    marketBalances[_order.tickerSymbol].demand) -
                    marketBalances[_order.tickerSymbol].borrowed;
            if (!shortingProhibited) {
                if (borrowable >= 0) {
                    if (borrowable >= _order.amountOfShares) {
                        _order.isFilled = true;
                        emit OrderFilled(
                            _order.orderId,
                            _order.shareholder,
                            _order.tickerSymbol,
                            _order.amountOfShares
                        );
                        filledOrders.push(_order);
                    } else {
                        if (borrowable > 0) {
                            _order.amountOfShares -= borrowable;

                            emit OrderPartiallyFilled(
                                _order.orderId,
                                _order.shareholder,
                                _order.tickerSymbol,
                                borrowable
                            );

                            emit OrderReduced(
                                _order.orderId,
                                _order.shareholder,
                                _order.tickerSymbol,
                                _order.amountOfShares
                            );

                            Order memory filledPart =
                                Order({
                                    orderId: order_ids++,
                                    time: block.timestamp,
                                    typeOfOrder: OrderType_SHORT,
                                    shareholder: _order.shareholder,
                                    tickerSymbol: _order.tickerSymbol,
                                    amountOfShares: borrowable,
                                    price: _order.price,
                                    isValue: true,
                                    isFilled: true
                                });
                            filledOrders.push(filledPart);
                        }
                    }
                }
            } else {
                emit ShortingProhibited();
            }
        }
    }

    /** @dev adds an order for a shareholder. Note: for testing purposes, Order types ar encoded as follows:
     * 0 =  LONG, 1 = SELL, anything else = SHORT.
     */
    function addOrderForShareholder(
        address _addr,
        uint256 _orderType,
        string memory _ticker,
        uint256 _price,
        uint256 _noOfShares
    ) public mustBeShareholder(_addr) returns (Order[] memory) {
        delete filledOrders;
        Order memory order =
            Order({
                orderId: order_ids++,
                time: block.timestamp,
                typeOfOrder: _orderType,
                shareholder: _addr,
                tickerSymbol: _ticker,
                price: _price,
                amountOfShares: _noOfShares,
                isValue: true,
                isFilled: false
            });
        Order[] storage filled = _matchMarketBalanceToOrder(order);
        orderBook[_addr].push(order);
        emit NumberOfOrdersFilled(filled.length);
        if (filled.length > 0) {
            for (uint256 i = 0; i < filled.length; i++) {
                Investor investor =
                    Investor(payable(address(filled[i].shareholder)));
                for (uint256 j = 0; j < filled.length; j++)
                    investor.handleAccountBalances(filled[j]);
            }
        }
        if (shortingProhibited) {
            emit ShortingProhibited();
        } else {
            emit NewOrderForShareholder(_addr, order);
        }
        return filled;
    }

    function provideOrdersOfLastDay()
        public
        canBeCalledOnlyBySEC()
        returns (Order[] memory)
    {
        delete totalOrders;
        for (uint256 i = 0; i < shareholders.length; i++) {
            for (uint256 j = 0; j < orderBook[shareholders[i]].length; j++) {
                if (
                    orderBook[shareholders[i]][j].time > block.timestamp - 86400
                ) totalOrders.push(orderBook[shareholders[i]][j]);
            }
        }
        return totalOrders;
    }
}
