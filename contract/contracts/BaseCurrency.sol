// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "./Broker.sol";

contract BaseCurrency is ERC20 {
    constructor() ERC20("BaseCurrency", "BSC") {}

    uint256 private USD_TO_WEI = 240000000000000;
    mapping(string => Stock) private enlistedStocks;
    string[] private enlistedTickers;
    address private secAddress;

    struct Stock {
        string tickerSymbol;
        uint256 totalOutstandingShares;
        uint256 borrowable;
        bool additionalOfferingsAllowed;
        uint256 decimals;
        uint256 price;
        uint256 tickerIndex;
        bool isValue;
    }

    /** @dev gets fired whenever a new enlisting happens */
    event NewEnlisting(
        address enlister,
        uint256 amount,
        string ticker,
        uint256 listingPrice
    );
    event Received(address _addr, uint256 _amount);

    event StockDetailsModified(string _ticker);

    modifier isEnlisted(string memory _ticker) {
        require(
            !enlistedStocks[_ticker].isValue,
            "There is already a stock listed with this ticker"
        );
        _;
    }

    modifier hasEnoughStarterShares(uint256 _initialShareCount) {
        require(
            _initialShareCount >= 1000,
            "Please opt for at least 1000 initial shares"
        );
        _;
    }

    modifier priceIsValid(uint256 _listingPrice) {
        require(
            _listingPrice <= 50000000 && _listingPrice > 0,
            "Please provide a realistic share price. Note that a share price of 100 will be displayed as 1.00 BS"
        );
        _;
    }

    modifier mustBeCovered(uint256 _amount) {
        require(
            msg.sender.balance >= _amount * USD_TO_WEI,
            "You need to cover the minting process with enough Wei. 1 USD = 1 BSC"
        );
        _;
    }

    modifier canbeCalledOnlyBySec() {
        require(msg.sender == secAddress, "Using this method is prohibited!");
        _;
    }

    fallback() external payable {
        // fallback to receive and store ETH
    }

    receive() external payable {
        // fallback to receive and store ETH
        emit Received(msg.sender, msg.value);
    }

    /**
     * @dev returns the ticker list as proof of listings.
     */
    function getEnlistedStockList() public view returns (string[] memory) {
        return enlistedTickers;
    }

    function getStockDetailsForTicker(string memory _ticker)
        public
        view
        canbeCalledOnlyBySec()
        returns (Stock memory)
    {
        return enlistedStocks[_ticker];
    }

    function setSecAddress(address _secAddress) public {
        secAddress = _secAddress;
    }

    function updateStockDetails(Stock memory _stock)
        public
        canbeCalledOnlyBySec()
    {
        enlistedStocks[_stock.tickerSymbol] = _stock;
        emit StockDetailsModified(_stock.tickerSymbol);
    }

    /**
     * @dev Enlists a specific stock with an unique ticker and mints enough BSC to the enlisting address to cover
     * the stock value. Accepts Wei and mints the exact amount of BSC to the sending address.
     */
    function enlistStock(
        string memory _ticker,
        uint256 _initialShareCount,
        uint256 _listingPrice,
        address _listingBrokerAddr,
        address _enlisterAddress
    )
        public
        payable
        isEnlisted(_ticker)
        hasEnoughStarterShares(_initialShareCount)
        priceIsValid(_listingPrice)
    {
        enlistedTickers.push(_ticker);

        Stock memory newStock =
            Stock({
                tickerSymbol: _ticker,
                totalOutstandingShares: _initialShareCount,
                price: _listingPrice,
                borrowable: _initialShareCount,
                decimals: 2,
                additionalOfferingsAllowed: true,
                tickerIndex: enlistedTickers.length - 1,
                isValue: true
            });
        enlistedStocks[newStock.tickerSymbol] = newStock;
        _mint(
            msg.sender,
            ((_listingPrice * _initialShareCount) * USD_TO_WEI) / 100
        );
        Broker broker = Broker(_listingBrokerAddr);
        broker.addSupplyAfterListing(
            newStock.tickerSymbol,
            newStock.price,
            newStock.totalOutstandingShares,
            _enlisterAddress
        );
        emit NewEnlisting(msg.sender, msg.value, _ticker, _listingPrice);
    }

    /**
     * @dev mints a requested amount of BSC to a trader address. Returns true if the balance of the requested address is >= than the requested amount.
     */
    function mintBSCToTrader(uint256 _amount) public payable returns (bool) {
        increaseAllowance(msg.sender, _amount);
        _mint(msg.sender, _amount);
        if (balanceOf(msg.sender) >= _amount) {
            return true;
        } else {
            return false;
        }
    }

    function transferBSC(
        address _sender,
        address _receiver,
        uint256 _amount
    ) public payable {
        increaseAllowance(_sender, _amount);
        transferFrom(_sender, _receiver, _amount);
    }

    function isTickerValid(string memory _ticker) public view returns (bool) {
        if (enlistedStocks[_ticker].isValue) {
            return true;
        } else {
            return false;
        }
    }
}
