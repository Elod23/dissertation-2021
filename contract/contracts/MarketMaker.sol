// SPDX-License-Identifier: MIT
pragma solidity 0.8.4;
pragma experimental ABIEncoderV2;

import "./Broker.sol";
import "./Investor.sol";
import "./BaseCurrency.sol";

contract MarketMaker is Investor {
    constructor(uint256 _spread) Investor(0, 0, "Market", "Maker") {
        spread = _spread;
        sellsShares = true;
        buysShares = true;
    }

    uint256 private spread;
    bool private buysShares;
    bool private sellsShares;

    function getSpread() external view returns (uint256) {
        return spread;
    }

    function provideBuyerLiquidity(
        string memory _ticker,
        uint256 _requestedShares,
        uint256 _bid,
        address _brokerAddress,
        address _bscAddress
    ) external payable {
        if (buysShares) {
            long(_ticker, _requestedShares, _bid, _brokerAddress, _bscAddress);
        }
    }

    function provideSellerLiquidity(
        string memory _ticker,
        uint256 _givenShares,
        uint256 _ask,
        address _brokerAddress
    ) external payable {
        if (sellsShares) {
            sell(_ticker, _givenShares, _ask, _brokerAddress);
        }
    }
}
