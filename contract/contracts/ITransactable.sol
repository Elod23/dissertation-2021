// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

interface ITransactable {
    function long(
        string memory _ticker,
        uint256 _requestedShares,
        uint256 _bid,
        address _brokerAddress,
        address _bscAddress
    ) external payable;

    function sell(
        string memory _ticker,
        uint256 _givenShares,
        uint256 _ask,
        address _brokerAddress
    ) external payable;

    function short(
        string memory _ticker,
        uint256 _requestedShares,
        uint256 _ask,
        address _brokerAddress
    ) external;

    function closeShortPosition(
        string memory _ticker,
        uint256 _orderId,
        address _brokerAddress,
        address _bscAddress
    ) external payable;
}
