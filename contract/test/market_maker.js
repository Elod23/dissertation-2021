const MarketMaker = artifacts.require("MarketMaker");
const Investor = artifacts.require("Investor");
const BaseCurrency = artifacts.require("BaseCurrency");
const Broker = artifacts.require("Broker");
const truffleAsserts = require("truffle-assertions");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */

contract("MarketMaker test suite", (accounts) => {
  let instanceMM;
  let instanceInvestor;
  let bsc;
  let broker;
  const spread = 5;
  const USD_TO_WEI = 240000000000000;
  const ticker = "HCMC";
  const shareNumber = 100;
  const price = 100;

  describe("Init and base tests", () => {
    beforeEach("should setup the contracts", async () => {
      instanceMM = await MarketMaker.deployed();
      console.log("MM address: " + instanceMM.address);
      instanceInvestor = await Investor.deployed();
      console.log("Investor address: " + instanceInvestor.address);
      bsc = await BaseCurrency.deployed();
      console.log("BSC address: " + bsc.address);
      broker = await Broker.deployed();
      console.log("Broker address: " + broker.address);
    });

    it("Should return the broker spread", async () => {
      const spreadOfMM = await instanceMM.getSpread();
      assert.equal(spread, spreadOfMM);
    });
  });

  describe("Market Maker simulation", () => {
    describe("Setup steps:", () => {
      it("Market Maker gets 1 mil BSC", async () => {
        const initBalanceMM = await bsc.balanceOf(instanceMM.address);
        assert.equal("0", initBalanceMM.toString());
        const initBalanceInvestor = await bsc.balanceOf(
          instanceInvestor.address
        );
        assert.equal("0", initBalanceInvestor.toString());

        const resultM = await instanceMM.obtainBSC(bsc.address, 1000000, {
          value: 1000000 * USD_TO_WEI,
          from: accounts[1],
        });

        const resultI = await instanceInvestor.obtainBSC(bsc.address, 100, {
          value: 100 * USD_TO_WEI,
          from: accounts[1],
        });
        const investorBalanceAfter = await bsc.balanceOf(
          instanceInvestor.address
        );
        const mmBalanceAfter = await bsc.balanceOf(instanceMM.address);
        assert.equal(mmBalanceAfter.toString(), "1000000");
        assert.equal(investorBalanceAfter.toString(), "100");
        truffleAsserts.eventEmitted(resultI, "BSCMinted");
        truffleAsserts.eventEmitted(resultM, "BSCMinted");
      });
      it("Enlist at broker => ", async () => {
        await instanceMM.enlistAtBroker(broker.address, broker.getName());
        await instanceInvestor.enlistAtBroker(broker.address, broker.getName());
        const isListed = await broker.getShareholders();
        assert.equal(isListed.length, 2);
      });
    });
    describe("Market activities", () => {
      it("First MM provides SELL, LONG order matches", async () => {
        const investorBalance = await bsc.balanceOf(instanceInvestor.address);
        const mmBalance = await bsc.balanceOf(instanceMM.address);

        console.log("Investor balance: " + investorBalance);
        console.log("MM balance: " + mmBalance);

        const resultMM = await instanceMM.provideSellerLiquidity(
          ticker,
          shareNumber,
          price,
          broker.address
        );

        const result = await instanceInvestor.long(
          ticker,
          shareNumber,
          price,
          broker.address,
          bsc.address
        );

        const investorBalanceAfter = await bsc.balanceOf(
          instanceInvestor.address
        );
        const mmBalanceAfter = await bsc.balanceOf(instanceMM.address);

        console.log("Investor balance: " + investorBalanceAfter);
        console.log("MM balance: " + mmBalanceAfter);
        assert.equal("1000100", mmBalanceAfter.toString());
        assert.equal("0", investorBalanceAfter.toString());
        //check orders for shareholders
        const ordersOfMM = await broker.getOrdersForShareholder(
          instanceMM.address
        );
        const ordersOfInvestor = await broker.getOrdersForShareholder(
          instanceInvestor.address
        );
        assert.equal(ordersOfInvestor.length, 1);
        assert.equal(ordersOfMM.length, 1);
        assert.isTrue(ordersOfInvestor[0].isValue);
        assert.isTrue(ordersOfMM[0].isValue);
      });

      it("First MM provides LONG, SELL order matches", async () => {
        const investorBalance = await bsc.balanceOf(instanceInvestor.address);
        const mmBalance = await bsc.balanceOf(instanceMM.address);

        console.log("Investor balance: " + investorBalance);
        console.log("MM balance: " + mmBalance);

        const resultMM = await instanceMM.provideBuyerLiquidity(
          ticker,
          shareNumber,
          price,
          broker.address,
          bsc.address
        );

        const result = await instanceInvestor.sell(
          ticker,
          shareNumber,
          price,
          broker.address
        );

        const investorBalanceAfter = await bsc.balanceOf(
          instanceInvestor.address
        );
        const mmBalanceAfter = await bsc.balanceOf(instanceMM.address);

        console.log("Investor balance: " + investorBalanceAfter);
        console.log("MM balance: " + mmBalanceAfter);
        assert.equal("1000100", mmBalanceAfter.toString());
        assert.equal("0", investorBalanceAfter.toString());
        //check orders for shareholders
        const ordersOfMM = await broker.getOrdersForShareholder(
          instanceMM.address
        );
        const ordersOfInvestor = await broker.getOrdersForShareholder(
          instanceInvestor.address
        );
        assert.equal(ordersOfInvestor.length, 2);
        assert.equal(ordersOfMM.length, 2);
        assert.isTrue(ordersOfInvestor[0].isValue);
        assert.isTrue(ordersOfInvestor[1].isValue);
        assert.isTrue(ordersOfMM[0].isValue);
        assert.isTrue(ordersOfMM[1].isValue);
      });
    });
  });
});
