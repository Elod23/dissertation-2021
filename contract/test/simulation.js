const SEC = artifacts.require("SEC");
const BaseCurrency = artifacts.require("BaseCurrency");
const Broker = artifacts.require("Broker");
const Investor = artifacts.require("Investor");
const SecondInvestor = artifacts.require("SecondInvestor");
const MarketMaker = artifacts.require("MarketMaker");
const truffleAsserts = require("truffle-assertions");
/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("Decentralized Stock Exchange Simulation:", (accounts) => {
  let sec;
  let marketMaker;
  let investor;
  let secondInvestor;
  let broker;
  let baseCurrency;

  const apple = "AAPL";
  const appleListingPrice = 20000;
  const googleListingPrice = 10000;
  const google = "GOOGL";
  const USD_TO_WEI = 240000000000000;
  before("Init contracts:", async () => {
    sec = await SEC.deployed();
    marketMaker = await MarketMaker.deployed();
    broker = await Broker.deployed();
    investor = await Investor.deployed();
    secondInvestor = await SecondInvestor.deployed();
    baseCurrency = await BaseCurrency.deployed();
  });
  describe("Preparing SEC and BSC contracts for use:", () => {
    it("Making them known for each other", async () => {
      await sec.setSelfAddressForTests(sec.address);
      await baseCurrency.setSecAddress(sec.address);
      await sec.setBscAddress(baseCurrency.address);
    });
    it("Enlisting our broker", async () => {
      await sec.enlistBroker(broker.address);
      const secAddress = await broker.getSEC();
      assert.equal(sec.address, secAddress);
    });
  });
  describe("Enlist our two investors and the market maker:", () => {
    it("Listing process", async () => {
      await investor.enlistAtBroker(broker.address, broker.getName());
      await secondInvestor.enlistAtBroker(broker.address, broker.getName());
      await marketMaker.enlistAtBroker(broker.address, broker.getName());
      const isListed = await broker.getShareholders();
      assert.equal(isListed.length, 3);
    });
  });
  describe(apple + " and " + google + " get enlisted by our MM:", () => {
    it(apple + " listing", async () => {
      const result = await baseCurrency.enlistStock(
        apple,
        1000000,
        appleListingPrice,
        broker.address,
        marketMaker.address
      );
      truffleAsserts.eventEmitted(result, "NewEnlisting");
    });
    it(google + " listing", async () => {
      const result = await baseCurrency.enlistStock(
        google,
        1000000,
        googleListingPrice,
        broker.address,
        marketMaker.address
      );
      truffleAsserts.eventEmitted(result, "NewEnlisting");
    });
  });
  describe("Trading starts:", () => {
    it("Initialize the Investors with 10000 BSC each", async () => {
      const mintingFirst = await investor.obtainBSC(
        baseCurrency.address,
        10000,
        {
          value: 10000 * USD_TO_WEI,
          from: accounts[1],
        }
      );
      truffleAsserts.eventEmitted(mintingFirst, "BSCMinted");
      const mintingSecond = await secondInvestor.obtainBSC(
        baseCurrency.address,
        10000,
        {
          value: 10000 * USD_TO_WEI,
          from: accounts[1],
        }
      );
      truffleAsserts.eventEmitted(mintingSecond, "BSCMinted");
      const balanceAfter = await baseCurrency.balanceOf(investor.address);
      const balanceAfter2 = await baseCurrency.balanceOf(
        secondInvestor.address
      );
      assert.equal(balanceAfter.toString(), balanceAfter2.toString());
      assert.equal("10000", balanceAfter.toString());
    });
    it(
      "Our first investor puts up 5 LONG orders for 1 share each at listing price for " +
        apple,
      async () => {
        for (let i = 0; i <= 5; i++) {
          await investor.long(
            apple,
            1,
            appleListingPrice,
            broker.address,
            baseCurrency.address
          );
        }
      }
    );
    it(
      "Our second investor puts up 5 LONG orders for 1 shares each at listing price also for " +
        google,
      async () => {
        for (let i = 0; i <= 5; i++) {
          await secondInvestor.long(
            google,
            1,
            googleListingPrice,
            broker.address,
            baseCurrency.address
          );
        }
      }
    );
    it(
      "Our market maker is shorting 5 * 1000 shares of " + google,
      async () => {
        for (let i = 0; i <= 5; i++) {
          await marketMaker.short(
            google,
            1000,
            googleListingPrice,
            broker.address
          );
        }
      }
    );
    it("Because of that, the share price drops, and our second investor sells their shares for a loss", async () => {
      await secondInvestor.sell(
        google,
        5,
        googleListingPrice - 1000,
        broker.address
      );
    });
    it("Our market maker closes the shorts, gaining money", async () => {
      const balanceOfMMBefore = await baseCurrency.balanceOf(
        marketMaker.address
      );
      const shorts = await marketMaker.getShortsForTicker(google);
      shorts.forEach(async (short) => {
        await marketMaker.closeShortPosition(
          short.tickerSymbol,
          short.orderId,
          broker.address,
          baseCurrency.address
        );
      });
      const balanceOfMMAfter = await baseCurrency.balanceOf(
        marketMaker.address
      );
      assert.isTrue(balanceOfMMAfter > balanceOfMMBefore);
    });
    it("Our market maker shorts 1000 shares of " + apple + " too", async () => {
      await marketMaker.short(apple, 1000, appleListingPrice, broker.address);
    });
    it("Because of that, the share price drops, and our first investor sells their shares for a loss", async () => {
      await investor.sell(apple, 50, appleListingPrice - 8000, broker.address);
    });
  });
  describe("The day ends, the SEC analyses the market activities:", () => {
    it(
      "Seeing all the downwards pressure from our MM, the SEC halts shorting on " +
        google,
      async () => {
        await sec.enableSuspiciousActivity();
        await sec.analyseOrdersForTicker(google, broker.address);
        const stockDetails = await sec.getStockDetailsForTicker(google);
        assert.equal("0", stockDetails.borrowable.toString());
      }
    );
  });
  it(
    "Our Market Maker tries to short " + google + " again, without success",
    async () => {
      await marketMaker.enlistAtBroker(broker.address, broker.getName());
      const repeatedShort = await marketMaker.short(
        google,
        1000,
        googleListingPrice,
        broker.address
      );
      truffleAsserts.eventEmitted(repeatedShort, "NewShortOrder");
    }
  );
});
