const BaseCurrency = artifacts.require('BaseCurrency');
const Broker = artifacts.require('Broker');
const Investor = artifacts.require('Investor');
const truffleAsserts = require('truffle-assertions');
/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract('BaseCurrency test suite', (accounts) => {
  let instance;
  let broker;
  let investor;
  const contractName = 'BaseCurrency';
  const symbol = 'BSC';

  describe('Initialization', () => {
    beforeEach('should setup the contract instance', async () => {
      instance = await BaseCurrency.deployed();
      broker = await Broker.deployed();
      investor = await Investor.deployed();
    });

    it('Should return the contract name', async () => {
      const _contractName = await instance.name();
      assert.equal(_contractName, contractName);
    });

    it('Should return the contract symbol', async () => {
      const _contractName = await instance.symbol();
      assert.equal(_contractName, symbol);
    });

    it('Should return the empty stock list', async () => {
      const stockList = await instance.getEnlistedStockList();
      assert.equal(stockList.length, 0);
    });
  });

  describe('Testing enlisting of stocks', () => {
    it('Should execute for ticker and emit NewEnlisting and Transfer', async () => {
      const result = await instance.enlistStock(
        'FTC',
        10000,
        40,
        broker.address,
        investor.address
      );
      truffleAsserts.eventEmitted(result, 'NewEnlisting');
      truffleAsserts.eventEmitted(result, 'Transfer');
      const stockList = await instance.getEnlistedStockList();
      assert.equal(stockList.length, 1);
    });

    it('Should execute for ticker and emit two events and pretty print them', async () => {
      const result = await instance.enlistStock(
        'STC',
        10000,
        40,
        broker.address,
        investor.address
      );
      truffleAsserts.prettyPrintEmittedEvents(result);
      const stockList = await instance.getEnlistedStockList();
      assert.equal(stockList.length, 2);
    });

    it('Should fail for duplicate ticker', async () => {
      await truffleAsserts.reverts(
        instance.enlistStock('STC', 10000, 40, broker.address, investor.address)
      );
    });

    it('Should fail for too low share count', async () => {
      await truffleAsserts.reverts(
        instance.enlistStock('TTC', 1, 40, broker.address, investor.address)
      );
    });

    it('Should fail for smaller and bigger listing price than allowed', async () => {
      await truffleAsserts.reverts(
        instance.enlistStock('TTC', 10000, 0, broker.address, investor.address)
      );
      await truffleAsserts.reverts(
        instance.enlistStock(
          'TTC',
          10000,
          50000001,
          broker.address,
          investor.address
        )
      );
    });

    it('Should execute for ticker and check enlsited stock tickers', async () => {
      const result = await instance.enlistStock(
        'TTC',
        10000,
        40,
        broker.address,
        investor.address
      );
      truffleAsserts.prettyPrintEmittedEvents(result);
      const stockList = await instance.getEnlistedStockList();
      assert.equal(stockList.length, 3);

      const firstTicker = stockList[0];
      const secondTicker = stockList[1];
      const thirdTicker = stockList[2];
    });
  });
});
