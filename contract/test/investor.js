const Investor = artifacts.require("Investor");
const SecondInvestor = artifacts.require("SecondInvestor");
const BaseCurrency = artifacts.require("BaseCurrency");
const Broker = artifacts.require("Broker");
const truffleAsserts = require("truffle-assertions");
/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("Investor", (accounts) => {
  let instance;
  let bsc;
  let broker;
  const USD_TO_WEI = 240000000000000;
  const ticker = "HCMC";
  const shareNumber = 100;
  let bid = 130;
  let seller;

  describe("Setup", () => {
    beforeEach(
      "Initializes the investor contract with zero ETH and sends wei",
      async () => {
        instance = await Investor.deployed();
        bsc = await BaseCurrency.deployed();
        broker = await Broker.deployed();
        const balance = await web3.eth.getBalance(instance.address);
        console.log("Balance: " + balance);
        seller = await SecondInvestor.deployed();
        console.log("Buyer contract address is: " + instance.address);
        console.log("Seller contract address is: " + seller.address);

        let the_eth = web3.utils.toWei("1", "ether");
        await web3.eth.sendTransaction({
          from: accounts[0],
          to: instance.address,
          value: the_eth,
        });
        let balance_wei = await web3.eth.getBalance(instance.address);
        let balance_ether = web3.utils.fromWei(balance_wei.toString(), "ether");
        assert.isTrue(balance_ether > 0);
      }
    );

    it("Getters and setters", async () => {
      const dep_ok = await instance.hasRegistered();
      let broker = await instance.getBroker();

      const ownedTickers = await instance.getOwnedTickers();
      assert.isTrue(dep_ok);
      assert.equal("", broker);
      assert.equal(0, ownedTickers.length);

      await instance.setBroker("Interactive Brokers");

      broker = await instance.getBroker();

      assert.equal("Interactive Brokers", broker);
    });
  });

  describe("Transactions: ", () => {
    it("BSC allocation", async () => {
      const initBalance = await bsc.balanceOf(instance.address);

      const result = await instance.obtainBSC(
        bsc.address,
        (bid / 100) * shareNumber,
        {
          value: (bid / 100) * shareNumber * USD_TO_WEI,
          from: accounts[0],
        }
      );
      const balanceAfter = await bsc.balanceOf(instance.address);

      assert.equal(balanceAfter.toString(), "130");
      assert.isTrue(balanceAfter > initBalance);
      assert.isTrue(balanceAfter - initBalance === (bid / 100) * shareNumber);
      truffleAsserts.eventEmitted(result, "BSCMinted");
    });

    describe("Market activities: ", () => {
      it("Enlist at broker => ", async () => {
        await instance.enlistAtBroker(broker.address, broker.getName());
        await seller.enlistAtBroker(broker.address, broker.getName());
        const isListed = await broker.getShareholders();
        assert.equal(isListed.length, 2);
      });

      it("A LONG order happens", async () => {
        // check broker
        const brokerName = await instance.getBroker();

        assert.isTrue(brokerName != null);

        // check balance to be enough
        const bscBalance = await bsc.balanceOf(instance.address);

        assert.equal("130", bscBalance.toString());

        console.log("Balance in BSC => " + bscBalance);

        // call the long function
        const result = await instance.long(
          ticker,
          shareNumber,
          bid,
          broker.address,
          bsc.address
        );
        // check balance

        const bscBalanceAfter = await bsc.balanceOf(instance.address);
        console.log("After => " + bscBalanceAfter);

        //check orders for shareholder
        const ordersOfShareholder = await broker.getOrdersForShareholder(
          instance.address
        );
        assert.equal("130", bscBalanceAfter.toString());
        assert.equal(ordersOfShareholder.length, 1);
        assert.isTrue(ordersOfShareholder[0].isValue);
      });

      it("A SELL order happens and a long comes in with matching price", async () => {
        await seller.enlistAtBroker(broker.address, broker.getName());

        const bscBalance = await bsc.balanceOf(seller.address);
        const buyerBscBalance = await bsc.balanceOf(instance.address);

        console.log("Seller balance: " + bscBalance);
        console.log("Buyer balance: " + buyerBscBalance);
        // call the sell function
        await seller.sell(ticker, shareNumber + 200, bid, broker.address);

        await instance.long(
          ticker,
          shareNumber,
          bid,
          broker.address,
          bsc.address
        );

        // check events
        // truffleAsserts.eventEmitted(result, 'NewOrderForShareholder');
        // check balance of seller
        const bscBalanceAfterSell = await bsc.balanceOf(seller.address);
        const bscBalanceAfterBuy = await bsc.balanceOf(instance.address);

        console.log("Balance after BUY =>" + bscBalanceAfterBuy);
        assert.equal(
          buyerBscBalance - (shareNumber * bid) / 100,
          bscBalanceAfterBuy
        );
        assert.equal();

        console.log("Balance after SELL =>" + bscBalanceAfterSell);
        assert.equal((shareNumber * bid) / 100, bscBalanceAfterSell);

        // check balance for buyer
        //check orders for shareholders
        const ordersOfSeller = await broker.getOrdersForShareholder(
          seller.address
        );
        const ordersOfBuyer = await broker.getOrdersForShareholder(
          instance.address
        );
        assert.equal(ordersOfSeller.length, 1);
        assert.equal(ordersOfBuyer.length, 2);
        assert.isTrue(ordersOfBuyer[0].isValue);
        assert.isTrue(ordersOfBuyer[1].isValue);
        assert.isTrue(ordersOfSeller[0].isValue);
      });

      it("A refill of BSC comes to the long investor", async () => {
        const balanceBefore = await bsc.balanceOf(instance.address);
        assert.equal("0", balanceBefore.toString());
        const totalSupplyBefore = await bsc.totalSupply();
        const result = await instance.obtainBSC(
          bsc.address,
          ((shareNumber + 200) * bid) / 100,
          {
            value: (((shareNumber + 200) * bid) / 100) * USD_TO_WEI,
            from: accounts[0],
          }
        );
        const totalSupplyAfter = await bsc.totalSupply();
        assert.equal(
          ((shareNumber + 200) * bid) / 100,
          totalSupplyAfter - totalSupplyBefore
        );
        truffleAsserts.eventEmitted(result, "BSCMinted");

        const balanceAfter = await bsc.balanceOf(instance.address);
        assert.equal(((shareNumber + 200) * bid) / 100, balanceAfter);
      });

      it("Another sell order comes in and gets matched by two buys", async () => {
        const sellerBscBalance = await bsc.balanceOf(seller.address);
        const buyerBscBalance = await bsc.balanceOf(instance.address);
        console.log("Seller balance: " + sellerBscBalance);
        console.log("Buyer balance: " + buyerBscBalance);
        // call the sell function
        await seller.sell(ticker, shareNumber + 200, bid, broker.address);

        await instance.long(
          ticker,
          shareNumber,
          bid,
          broker.address,
          bsc.address
        );

        await instance.long(ticker, 200, bid, broker.address, bsc.address);

        // check balance of seller
        const bscBalanceAfterSell = await bsc.balanceOf(seller.address);
        const bscBalanceAfterBuy = await bsc.balanceOf(instance.address);
        console.log("Balance after BUY =>" + bscBalanceAfterBuy);
        assert.equal("0", bscBalanceAfterBuy.toString());

        console.log("Balance after SELL =>" + bscBalanceAfterSell);
        assert.equal("520", bscBalanceAfterSell.toString());

        // check balance for buyer
        //check orders for shareholders
        const ordersOfSeller = await broker.getOrdersForShareholder(
          seller.address
        );
        const ordersOfBuyer = await broker.getOrdersForShareholder(
          instance.address
        );
        assert.equal(ordersOfSeller.length, 2);
        assert.equal(ordersOfBuyer.length, 4);
        assert.isTrue(ordersOfBuyer[0].isValue);
        assert.isTrue(ordersOfBuyer[1].isValue);
        assert.isTrue(ordersOfBuyer[2].isValue);
        assert.isTrue(ordersOfBuyer[3].isValue);
        assert.isTrue(ordersOfSeller[0].isValue);
        assert.isTrue(ordersOfSeller[1].isValue);
      });

      it("A short order comes in", async () => {
        await seller.short(ticker, shareNumber, bid, broker.address);

        const result = await instance.short(
          ticker,
          shareNumber,
          bid,
          broker.address
        );

        truffleAsserts.eventEmitted(result, "NewShortOrder");
      });

      it("All shorts get closed for buyer and the balance is not changed since the market balances didnt change", async () => {
        const balanceBeforeShortClosing = await bsc.balanceOf(instance.address);
        const shorts = await instance.getShortsForTicker(ticker);
        shorts.forEach(async (short) => {
          console.log("Order id: " + short.orderId);
          await instance.closeShortPosition(
            short.tickerSymbol,
            short.orderId,
            broker.address,
            bsc.address
          );
        });
        const balanceOfBuyerAfter = await bsc.balanceOf(instance.address);
        assert.equal(
          balanceBeforeShortClosing.toString(),
          balanceOfBuyerAfter.toString()
        );
      });

      it("A sell comes in with lower price, seller closes shorts and wins money", async () => {
        const balanceBeforeShortClosing = await bsc.balanceOf(seller.address);
        console.log(
          "Balance before closing short positions:" + balanceBeforeShortClosing
        );
        await seller.sell(ticker, 400, bid - 50, broker.address);

        const shorts = await seller.getShortsForTicker(ticker);
        console.log("Shorts: " + shorts);

        shorts.forEach(async (short) => {
          console.log("Order id: " + short.orderId);
          const valu =
            (parseInt(short.price) * parseInt(short.shareAmount)) / 100;
          console.log("Short value << " + short.orderId + " >> " + valu);
          console.log("Order shares: " + short.shareAmount);
          await seller.closeShortPosition(
            short.tickerSymbol,
            short.orderId,
            broker.address,
            bsc.address
          );
        });
        const currentLowestAsk = await broker.getLowestAskForTicker(ticker);
        console.log("ask: " + currentLowestAsk);
        const balanceOfBuyerAfter = await bsc.balanceOf(seller.address);
        console.log("After closing shorts, balance is: " + balanceOfBuyerAfter);
        assert.isTrue(
          balanceOfBuyerAfter.toString() > balanceBeforeShortClosing.toString()
        );
      });
    });
  });
});
