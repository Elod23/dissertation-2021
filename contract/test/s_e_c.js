const SEC = artifacts.require("SEC");
const BaseCurrency = artifacts.require("BaseCurrency");
const Broker = artifacts.require("Broker");
const Investor = artifacts.require("Investor");
const truffleAsserts = require("truffle-assertions");
/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("SEC", (accounts) => {
  let secInstance;
  let bscInstance;
  let brokerInstance;
  let investorInstance;
  const ticker = "AAPL";
  const USD_TO_WEI = 240000000000000;
  before("Contract cleanup and deployment", async () => {
    secInstance = await SEC.deployed();
    console.log("SEC address: " + secInstance.address);
    bscInstance = await BaseCurrency.deployed();
    console.log("BSC address: " + bscInstance.address);
    brokerInstance = await Broker.deployed();
    console.log("Broker address: " + brokerInstance.address);
    investorInstance = await Investor.deployed();
    console.log("Investor address: " + investorInstance.address);
  });

  describe("Unit test for SEC", () => {
    it("Prepare self address for test", async () => {
      await secInstance.setSelfAddressForTests(secInstance.address);
      await bscInstance.setSecAddress(secInstance.address);
      const addr = await secInstance.getSelfAddress();
      console.log("Self: " + secInstance.address);
      console.log("Self assigned: " + addr);
    });
    it("Setting BSC address should be callable from the BSC contract address", async () => {
      await secInstance.setBscAddress(bscInstance.address);
    });
    it("Enlisting a new broker should succeed", async () => {
      await secInstance.enlistBroker(brokerInstance.address);
      const secAddress = await brokerInstance.getSEC();
      assert.equal(secInstance.address, secAddress);
    });
    describe(ticker + " gets enlisted at our broker!", () => {
      it("Enlisting should succeed: ", async () => {
        const result = await bscInstance.enlistStock(
          ticker,
          1000000,
          700,
          brokerInstance.address,
          investorInstance.address
        );
        truffleAsserts.eventEmitted(result, "NewEnlisting");
      });
    });
    describe("When suspicious activities detected, the SEC takes action", () => {
      it("New investor with 1000 BSC enlists at our broker", async () => {
        const minting = await investorInstance.obtainBSC(
          bscInstance.address,
          1000,
          {
            value: 1000 * USD_TO_WEI,
            from: accounts[0],
          }
        );
        truffleAsserts.eventEmitted(minting, "BSCMinted");

        await investorInstance.enlistAtBroker(
          brokerInstance.address,
          brokerInstance.getName()
        );
        const isListed = await brokerInstance.getShareholders();
        assert.equal(isListed.length, 1);
      });
      it("An investor creates multiple sell orders ans shorts at our broker", async () => {
        await investorInstance.long(
          ticker,
          100,
          100,
          brokerInstance.address,
          bscInstance.address
        );
        await investorInstance.long(
          ticker,
          100,
          100,
          brokerInstance.address,
          bscInstance.address
        );
        await investorInstance.sell(ticker, 100, 100, brokerInstance.address);
        await investorInstance.sell(ticker, 100, 100, brokerInstance.address);
        await investorInstance.short(ticker, 100, 100, brokerInstance.address);
        await investorInstance.short(ticker, 100, 100, brokerInstance.address);
        await investorInstance.short(ticker, 100, 100, brokerInstance.address);
      });
      describe("At the end of the day the SEC analyses the market activity:", () => {
        it("Stock gets analysed and not borrowable anymore", async () => {
          await secInstance.enableSuspiciousActivity();
          await secInstance.enableStockRestrictions();
          await secInstance.analyseOrdersForTicker(
            ticker,
            brokerInstance.address
          );
          const stockDetails = await secInstance.getStockDetailsForTicker(
            ticker
          );
          assert.equal("0", stockDetails.borrowable.toString());
          assert.equal(false, stockDetails.additionalOfferingsAllowed);
        });
      });
    });
  });
});
