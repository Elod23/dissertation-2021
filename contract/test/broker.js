const Broker = artifacts.require("Broker");
const truffleAsserts = require("truffle-assertions");
/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("Broker test suite", (accounts) => {
  let instance;
  let brokerName = "Broker";

  describe("Init", () => {
    beforeEach("Should set up the contract instance", async () => {
      instance = await Broker.deployed();
    });

    it("Should return name", async () => {
      const _name = await instance.getName();
      assert.equal(brokerName, _name);
    });

    it("Should check for no shareholders and tickers, empty orderBook and market balance", async () => {
      const _tickers = await instance.getTickers();
      const _shareholders = await instance.getShareholders();
      assert.equal(0, _tickers.length);
      assert.equal(0, _shareholders.length);
    });
  });

  describe("Basic functionality", () => {
    it("Should add a shareholder and emit NewShareholder", async () => {
      const _addr = instance.address;
      const _addedAShareholder = await instance.addShareholder(_addr);
      truffleAsserts.eventEmitted(_addedAShareholder, "NewShareholder");
      const _shareholders = await instance.getShareholders();
      assert.equal(1, _shareholders.length);
    });

    it("Should add an order for the already created shareholder", async () => {
      const _addr = instance.address;
      const _addedAnOrderForShareholder = await instance.addOrderForShareholder(
        _addr,
        0,
        "HCMC",
        130,
        100000
      );

      truffleAsserts.eventEmitted(
        _addedAnOrderForShareholder,
        "NewOrderForShareholder"
      );
      truffleAsserts.eventEmitted(
        _addedAnOrderForShareholder,
        "MarketBalanceUpdated"
      );

      const _shareholders = await instance.getShareholders();

      assert.equal(1, _shareholders.length);

      const _ordersForShareholder = await instance.getOrdersForShareholder(
        _addr
      );

      assert.equal(1, _ordersForShareholder.length);

      const _order = _ordersForShareholder[0];
      assert.equal(_order.shareholder, _addr);
      assert.equal(_order.tickerSymbol, "HCMC");
      assert.equal(_order.amountOfShares, 100000);
      assert.equal(_order.price, 130);
      assert.equal(_order.isValue, true);
    });

    it("Should check the market balance for the ticker", async () => {
      const ticker = "HCMC";
      const shareCount = 100000;
      const price = 130;
      const max_int = 2 ** 256 - 1;

      var response = await instance.getMarketBalanceForSecurity(ticker);

      assert.equal(0, response[0]);
      assert.equal(shareCount, response[1]);
      assert.equal(0, response[2]);
      assert.equal(price, response[3]);
      assert.equal(max_int, response[4]);
    });

    it("Should add a second order for the already created shareholder and same ticker", async () => {
      const _addr = instance.address;
      const _addedAnotherOrderForShareholder = await debug(
        instance.addOrderForShareholder(_addr, 1, "HCMC", 170, 111111)
      );

      truffleAsserts.eventEmitted(
        _addedAnotherOrderForShareholder,
        "NewOrderForShareholder"
      );
      truffleAsserts.eventEmitted(
        _addedAnotherOrderForShareholder,
        "MarketBalanceUpdated"
      );

      const _shareholders = await instance.getShareholders();
      assert.equal(1, _shareholders.length);

      const _ordersForShareholder = await instance.getOrdersForShareholder(
        _addr
      );
      assert.equal(2, _ordersForShareholder.length);
      const _order = _ordersForShareholder[1];
      assert.equal(_order.shareholder, _addr);
      assert.equal(_order.tickerSymbol, "HCMC");
      assert.equal(_order.amountOfShares, 111111);
      assert.equal(_order.price, 170);
      assert.equal(_order.isValue, true);
    });

    it("Should check the aggregated market balance for the ticker", async () => {
      const ticker = "HCMC";
      const shareCountDemand = 100000;
      const shareCountSupply = 111111;
      const priceD = 130;
      const priceS = 170;

      var response = await debug(instance.getMarketBalanceForSecurity(ticker));

      assert.equal(shareCountDemand, response[1]);
      assert.equal(shareCountSupply, response[0]);
      assert.equal(0, response[2]);
      assert.equal(priceD, response[3]);
      assert.equal(priceS, response[4]);
    });
  });
});
