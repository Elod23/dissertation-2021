const BaseCurrency = artifacts.require("BaseCurrency");
const Investor = artifacts.require("Investor");
const SecondInvestor = artifacts.require("SecondInvestor");
const MarketMaker = artifacts.require("MarketMaker");
const Broker = artifacts.require("Broker");
const SEC = artifacts.require("SEC");

module.exports = async (deployer) => {
  deployer.deploy(BaseCurrency);
  deployer.deploy(Investor, 0, 001, "First", "TestUser");
  deployer.deploy(SecondInvestor, 1, 002, "Second", "TestUser");
  deployer.deploy(Investor, 2, 003, "Third", "TestUser");
  deployer.deploy(Broker, "Broker");
  deployer.deploy(MarketMaker, 5);
  deployer.deploy(SEC);
};
