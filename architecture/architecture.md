# System Archtiecture

## General System design

The base function of the system is to:

- Make it possible for general managers to register on the platform.
- Make it possible to uplist on the stock exchange.
- Make it possible to issue/withdraw stock.
- Log in a persistent way every event ommitted by the smart contract that is regulating the base currency of the platform.

![General-purpose system outline](./general_system_outline.PNG)
