# Dissertation-2021

## Main logical blocks

**Note** : Only applicable to the test version.

### The BaseCurrency as store of value

The contract ecosystem, or the exchange, has the BaseCurrency.sol as its intra-eco currency. 1 BSC ~ 1 USD and is implemented using the ERC20 standard, more than enough for our use-case.
It's smart contract knows about the stocks, as an insider structure of data aggregation, which, in turn, makes the exact data about the stocks unknown for the other contracts, besides the SEC contract.  
Value transactions are handled by its contract, called from every other contract in the ecosystem.

**Note**: More clearly, in _DeFi_ terms, through BSC we tokenize the stocks.

#### Minting and obtaining BSC

There are two ways of obtaining BSC:

- Enlisting stocks costs wei, but at the end returns the exact amount in BSC. This is a way of rewarding institutions to enlist and motivate them to transact.
- For the test networks phase, investors can mint BSC for themselves, costing Wei.

### Investors as single responsible entities of value transactions

In the test world, Investor contracts are the only elements which have the power to start orders. As in the real life we hate when brokerages alter our holdings, in this _DeFi_ ecosystem, it is entirely impossible.

An Investors(or the one having access to its contract) can choose to do 3 types of orders:

- **Long**: The order in which they are obtaining a stock, which once matched triggers a BSC transaction.
- **Sell**: The order in which they are disposing a stock , which once matched triggers a BSC transaction.
- **Short**: The order in which they are borrowing an already existent share, which once closed triggers a BSC transaction.

**Note**: An Investor, once registered, must not change its address or risks losing hold of the orders associated with him.

**Note**: An Investor, once registered, needs to legally claim that the address is his/her and accessible only him/her during every and all transactions and orders made from it.

#### Orders vs Transactions

In technical terms orders are only ways of expressing willingness of transacting with a certain security.  
In the detailed implementation, the orders created by investors are not immediately transferring BSC, only when the brokers match an order to either a part of, or another order.

### Decoupling transfer of value from order matching

In our system, filling orders means matching the two sides of request and demand, while the end-peers successfully complete the value transfer between them.  
An order is successful only when both the order matching and the value transaction succeeds.

### Brokers as an order log not a carrier of value

As in the real centralized systems the brokerages are actually **transferring value**, in this ecosystem they are just a means to **connect investors**, thus creating a **log of orders** and matching them in **strict chronologial order**.  
Can you imagine this happening in the real world? Certainly not, but it is perfectly making sense in the _DeFi_ world. As I see it, there is literally no need for someone to transact our currencies when we're perfectly able to send it directly to our peers.

### Brokerages as a waterfall of event logging

In the real world, brokerages need to clear orders between each other to provide liquidity to the markets. This is realized by aggregating orders on different levels.

![Brokerage levels](./architecture/How the centralized system transacts stocks.PNG)

To have a certain level if liquidity means to carry the extra needed amount of order logs either from request or demand side from one prime- or sub-broker to another one, passing the coupling levels used in the real world by brokerage aggregations or DTCCs.

### DeX as problem solver

Our brokerage implementation is a perfect example of a decentralized exchange, which in turn can support several levels of order logging in one place, then writing those off to the underlying ledger's blocks.

### Prime brokers or market makers as liquidity providers

In the real world, market makers and large entities who are looking for minimal profits of trades on large volumes, while either holding or hooking into multiple brokerages.

### AMM as liquidity provider

To provide the necessary liquidity the setup leverages the functionalities of an automated market maker. In the real world, market makers obtain stock directly from the source or from anywhere else under market prices. This means that it is ensured, that market makers have the smallest entry point for both LONG and SHORT positions.

In the test setup, an automated market maker will be set up which is configured to automatically match orders, like:

- For SELL orders, the market makers would pick them up at market price.
  Then, they would automatically create a SELL order for market price + spread (or profit ratio).
- For LONG orders, the market makers would just provide your share at market price, given that they always obtain shares under market price.

**Note**: Regardless of it being true in the real world, this is just a hypothesis and should be treated as it is.

### Test environment

![Test setup](./architecture/Test-environment.PNG)

In our simulation environment I am aiminig for the minimal setup that emphasizes how trades are done in the real world system. Therefore, each system component will have a dedicated role:

- There will be 4 clients that are putting up orders, all of them creating both LONG, SELL and SHORT orders.

- Subsequently, two brokerages that are having 2-2 clients.

- Our market makers are going to look like investors, but will automatically respond to any unfilled orders that are staying in our brokerages' order logs more than an X amount of time, making at least the set minimal profit on every trade.(see the ProfitMargin attribute in broker.sol)

- Notice, that our test setup does not have a Clearing House. This is due to the nature of decentralized exchanges. The exchange is just logging orders, value transaction is not happening, thus it lifts the burden of tracking value. Regulatory organs can directly interrogate orders from the brokers and can act on possible breaches. Easy as that, we've eliminated such a big party of the classic exchange setup, because honestly, it became obsolete.

**Note** : There are regulatory organs created to track crypto transfers between wallets.

- The SEC will be the sole entity that has access to the full records of the stocks, adding bona fide contracts and changing the total share amounts, just like in the real world: any uplisting, shares offering or liquidity providing mechanism needs to be legally approved by the SEC, just that this way we don't have the human factor and approvals are transparent.

### Transparency and data encapsulation

This abstaction that takes away value transaction from brokerages add a strong level of accountability, because there is no real value inside the system, therefore exchange of properties happens only when a transaction succeeds between two investors.

This, in order, takes a unnecessary load of duty away from them and is making transparency natural, since the transactions themselves are perfectly public but the details of it aren't.

### GDPR and data compliance

In order to keep the system distributed and transparent, there's only one thing we need to do: keep the details from the order logs private, accessible only from regulatory contracts and from the contract that issued them. This is realized by the concept of **access by address** .

### Access by address

ABA is a contract or interface between **investors**, **brokers** and **regulatory organs**. It states that all parties agree that their transactions can be logged and traced back to their originating addresses, but their data can't be accessed only from that address. The regulatory organs do have access to the order data, but only in case of **suspicious market activities** or **market manipulation**.

### Regulation of markets and tracing of legislative constraints

The regulatory organs have full access to the records of orders, stripped to the bare minimum information needed to track **suspicious market activities** or **market manipulation**. For the test version of the system, this data is:

- The type of order issued (LONG, SHORT, SELL).
- The amount of securities at the stake of the order.
- The private information about the enlisted stocks:
  - The amount of outstanding shares.
  - The amount of borrowed shares of a security at a certain moment in the system.
  - Whether there is or isn't a bona fide contract issued for the security to one or more of the brokerages.

Using this data, the regulatory contracts can examine all market activities and evaluate whether a form of illegal activity happens in the market or not.

### Battling illegal activities

In case of suspicious illegal activities, the regulatory organs can ask for contract addresses of investors, thus tracing back order flows or transactions to the originating person.

### SEC contract as legislative body

In our ecosystem, the SEC contract serves as the one, centralized implementation of regulatory authorities in charge of supervising trades and order blocks.

## Interoperability and ease of migration

Since Solidity is used to develop the ecosystem, it is always the choice of the use-case to decide where to migrate it: Ethereum for full decentralization or Binance Smart Chain for smaller costs and faster runtime, even Ethereum 2.0 in the future.  
As Solidity starts to emerge, being the de-facto programming language for smart contract development has its undeniable perks, therefore I expectg it to need little to no change to be abel to migrate between crypto ledgers.

## Points of improvenet or thought log for next steps of impl:

- Create a ContractRegistry contract that has a mapping(struing => address) and contains the contracts your project depends on, like the BaseCurrency contract. Use this to keep them clean and tidy and query them by contract name easily.
- All \*\_broker contracts can be factories of investors contracts. Create a factory(pattern) main contract for them.
