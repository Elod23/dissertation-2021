package com.reselly.authorizationservice.entity;

import javax.persistence.*;

@Entity(name = "users")
@Table(name = "users")
public class Credentials {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String address;

    public Credentials() {
    }

    public Credentials(long id, String email, String password, String address) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Credentials{" + "email='" + email + '\'' + ", password='" + password + '\'' + '}';
    }
}
