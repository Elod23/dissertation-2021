import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Container from 'react-bootstrap/Container';

import Login from './storage/login';
import Register from './storage/register';
import Home from './storage/index';
import Profile from './storage/profile';
import Portfolio from './storage/Portfolio';

import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  state = {
    jwt: '',
    address: '',
  };
  componentDidMount() {
    const { history } = this.props;
    if (!this.state.jwt) {
      history.push('/login');
    }
  }

  passCreds = (jwt, address) => {
    console.log('Jwt set!');
    this.setState({ jwt: jwt, address: address });
  };

  signOut = () => {
    const { history } = this.props;
    this.setState({ jwt: undefined });
    history.push('/login');
  };

  goToProfile = () => {
    const { history } = this.props;
    history.push('/profile');
  };

  goTrade = () => {
    const { history } = this.props;
    history.push('/');
  };

  goToPortfolio = () => {
    const { history } = this.props;
    history.push('/portfolio');
  };

  render() {
    return (
      <div className='App'>
        <header>
          <Navbar bg='dark' variant='dark' expand='lg'>
            <Container>
              <Navbar.Brand>Home</Navbar.Brand>
              <Navbar.Toggle aria-controls='basic-navbar-nav' />
              <Navbar.Collapse id='basic-navbar-nav'>
                <Nav className='me-auto'>
                  <Nav.Link onClick={() => this.goTrade()}>Trade</Nav.Link>
                  <Nav.Link onClick={() => this.goToPortfolio()}>
                    My portfolio
                  </Nav.Link>
                  <NavDropdown
                    className='float-right'
                    title='Settings'
                    id='basic-nav-dropdown'>
                    <NavDropdown.Item onClick={() => this.goToProfile()}>
                      My Profile
                    </NavDropdown.Item>
                    <NavDropdown.Item onClick={() => this.signOut()}>
                      Exit
                    </NavDropdown.Item>
                  </NavDropdown>
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </header>
        <div>
          <Switch>
            <Route path='/login'>
              <Login passCreds={this.passCreds} />
            </Route>
            <Route path='/register'>
              <Register />
            </Route>
            <Route exact path='/'>
              <Home jwt={this.state.jwt} address={this.state.address} />
            </Route>
            <Route path='/profile'>
              <Profile jwt={this.state.jwt} />
            </Route>
            <Route path='/portfolio'>
              <Portfolio address={this.state.address} />
            </Route>
          </Switch>
        </div>
      </div>
    );
  }
}

export default withRouter(App);
