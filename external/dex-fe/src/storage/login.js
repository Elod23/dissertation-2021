import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { Jumbotron } from 'react-bootstrap';
import { Link, withRouter } from 'react-router-dom';

class Login extends Component {
  state = {
    email: '',
    password: '',
    redirect: false,
  };

  validateForm() {
    return (
      this.state.email.length > 3 &&
      this.state.email.includes('@') &&
      this.state.password.length >= 6
    );
  }

  setEmail = (value) => {
    this.setState({ email: value });
  };

  setPassword = (value) => {
    this.setState({ password: value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { history } = this.props;
    console.log(this.state.email + ' ' + this.state.password);
    fetch('/api/investors/credentials', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
    })
      .then((res, err) => {
        if (!err) {
          if (res.status !== '403' || res.status !== '400') {
            return res.json();
          }
        } else {
          return res.text().then((resp) => {
            throw new Error(resp.error);
          });
        }
      })
      .then((res) => {
        console.log(res);
        if (!res.message.includes('Invalid')) {
          this.props.passCreds(
            res,
            '0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4'
          );
          history.push('/');
        } else {
          alert('Invalid credentials!');
        }
      })
      .catch((err) => {
        console.log(err.message);
        alert('Somethign unexpected happened! 🥺 Please try again later!');
      });
  };

  render() {
    // if (this.state.redirect === true) {
    //   console.log('Redirecting...');
    //   return <Redirect to='/' />;
    // }
    return (
      <div className='Login'>
        <Container className={Jumbotron}>
          <Form onSubmit={() => this.handleSubmit()}>
            <Form.Group size='lg' controlId='email'>
              <Form.Label>Email</Form.Label>
              <Form.Control
                autoFocus
                type='email'
                value={this.state.email}
                onChange={(e) => this.setEmail(e.target.value)}
              />
            </Form.Group>
            <Form.Group size='lg' controlId='password'>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type='password'
                value={this.state.password}
                onChange={(e) => this.setPassword(e.target.value)}
              />
            </Form.Group>
            <Container>
              <br />
              <br />
              <div className={'col text-center'}>
                <Button
                  block
                  size='lg'
                  type='info'
                  disabled={!this.validateForm()}
                  onClick={(e) => this.handleSubmit(e)}>
                  Login
                </Button>
                <br />
                <br />
                <br />
                <br />
                <div>No account yet? {'  '}</div>
                <br />
                <Link className='btn btn-lg btn-info' to='/register'>
                  Register here
                </Link>
              </div>
            </Container>
          </Form>
        </Container>
      </div>
    );
  }
}

export default withRouter(Login);
