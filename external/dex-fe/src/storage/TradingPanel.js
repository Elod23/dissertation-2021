import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';

import Table from 'react-bootstrap/Table';
import InputOrder from '../components/InputOrder';

import Investor from '../components/investor';
import socketClient from '../io/socket';
import { Client } from '@stomp/stompjs';

const url = 'ws://localhost:15674/ws';

export default class TradingPanel extends Component {
  constructor(props) {
    super(props);
    if (url) {
      this.stompClient = new Client({
        brokerURL: url,
        connectHeaders: {
          login: 'full_access',
          passcode: 'elod23',
          durable: 'true',
          'auto-delete': 'false',
        },
        reconnectDelay: 5000,
        heartbeatIncoming: 4000,
        heartbeatOutgoing: 4000,
      });
      this.stompClient.onConnect = (frame) => {
        this.subscription = this.stompClient.subscribe(
          '/queue/0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4ordersorders',
          this.handleOrder
        );
      };
      this.stompClient.activate();
    }
    this.socket = socketClient();
    console.log('stomp: ' + this.stompClient);
    console.log('socket: ' + this.socket);
    this.socket.on('credentials', (message) => {
      console.log(message);
      this.setState({ investor: message });
    });
    this.socket.on('stocks', (message) => {
      console.log(message);
      if (message && Array.isArray(message)) {
        this.setState({ stocks: message.map((msg) => msg.ticker) });
      } else {
        if (message) {
          this.setState({ stocks: [message.ticker] });
        }
      }
    });
  }

  state = {
    jwt: '',
    newOrder: {},
    stocks: ['AAPL', 'GOOGL'],
    orders: [
      {
        id: '0',
        type: 'SELL',
        ticker: 'GOOGL',
        initiator: '0xcasdasda',
        shares: 12,
        price: 14,
        total: 12 * 14,
        filled: true,
      },
      {
        id: '1',
        type: 'SELL',
        ticker: 'AAPL',
        initiator: '0xcasdasda',
        shares: 17,
        price: 14,
        total: 17 * 14,
        filled: true,
      },
      {
        id: '2',
        type: 'LONG',
        ticker: 'HCMC',
        initiator: '0xcasdasda',
        shares: 100000,
        price: 0.00003,
        total: 100000 * 0.00003,
        filled: false,
      },
    ],
    investor: {},
    orderTypeInFocus: '',
    enlisted: false,
  };
  subscription = '';
  stompClient;
  socket;
  ORDER_SELL = 'SELL';
  ORDER_LONG = 'LONG';
  ORDER_SHORT = 'SHORT';
  newOrders = [];

  componentDidMount() {
    this._isMounted = true;
    this.socket.emit('stocks', this.props.address);
    console.log('asked for creds  for ' + this.props.address);
    this.setState({ jwt: this.props.jwt, address: this.state.address });
  }

  handleOrder = (message) => {
    const order = JSON.parse(message.body);
    console.log('new order');
    const clean = this.transformOrder(order);
    this.newOrders.push(clean);
    this.setState({
      orders: [...this.state.orders, ...this.newOrders],
    });
  };

  transformOrder = (order) => {
    switch (order.type) {
      case '1':
        order.type = this.ORDER_SELL;
        break;
      case '2':
        order.type = this.ORDER_SHORT;
        break;
      case '0':
        order.type = this.ORDER_LONG;
        break;
      default:
        order.type = this.ORDER_LONG;
    }
    order.id = order.externalOrderId;
    return order;
  };

  componentDidUpdate() {}

  componentWillUnmount() {
    this._isMounted = false;
    this.stompClient.deactivate();
  }

  isOpen = () => {
    return this.socket.readyState === this.socket.OPEN;
  };

  setOrderType = (e) => {
    this.setState({ orderTypeInFocus: e.target.value });
  };

  getOrderTypeInFocus = () => {
    if (this.state.orderTypeInFocus === '') {
      return '';
    } else {
      if (this.state.orderTypeInFocus === this.ORDER_LONG) {
        return this.ORDER_LONG;
      } else {
        if (this.state.orderTypeInFocus === this.ORDER_SELL) {
          return this.ORDER_SELL;
        } else {
          return this.ORDER_SHORT;
        }
      }
    }
  };

  enlistAtBroker = (e) => {
    e.preventDefault();
    Investor.enlistAtBroker().then((res, err) => {
      if (!err) {
        console.log('Enlisted!');
        alert('Successfully enlisted at broker!');
        console.log(res);
        if (this.isOpen()) {
          console.log('open');
          this.socket.emit('needCredentials', this.props.address);
          this.socket.emit('needOrders', this.props.address + 'orders');
          console.log('asked for orders to address: ' + this.props.address);
        }

        this.setState({ enlisted: true });
      } else {
        console.error(err);
      }
    });
  };

  sendOrder = (order) => {
    const beOrder = {
      id: this.state.orders.length + 1,
      initiator: this.state.investor.address,
      ticker: order.ticker,
      type: order.type,
      price: order.price,
      shares: order.shares,
      total: order.price * order.shares,
      filled: false,
    };
    console.log('emitting new order: ' + beOrder);
    this.socket.emit('newOrder', beOrder);
    const orders = this.state.orders;
    orders.push(beOrder);
    this.setState({ orders: orders });
  };

  fillOrderTable = () => {
    const orderType = this.getOrderTypeInFocus();
    let orders;
    if (orderType !== '') {
      orders = this.state.orders.filter((order) => order.type === orderType);
    } else {
      orders = this.state.orders;
    }
    return orders.map((order) => (
      <tr key={order.id}>
        <td>{order.type}</td>
        <td>{order.ticker}</td>
        <td>{order.initiator}</td>
        <td>{order.shares}</td>
        <td>{order.price}</td>
        <td>{order.price * order.shares}</td>
        <td>{order.filled ? '✔️' : '❌'}</td>
      </tr>
    ));
  };

  render() {
    return (
      <div className='tradingPanel'>
        <Button
          className='btn btn-md btn-info'
          onClick={this.setOrderType}
          value={this.ORDER_LONG}>
          {this.ORDER_LONG}
        </Button>
        <Button
          className='btn btn-md btn-danger'
          onClick={this.setOrderType}
          value={this.ORDER_SELL}>
          {this.ORDER_SELL}
        </Button>
        <Button
          className='btn btn-md btn-warning'
          onClick={this.setOrderType}
          value={this.ORDER_SHORT}>
          {this.ORDER_SHORT}
        </Button>
        <Button
          className='btn btn-md btn-info'
          onClick={this.enlistAtBroker}
          disabled={this.state.enlisted}
          value={this.state.enlisted}>
          {'Enlist at broker'}
        </Button>
        <br />
        <InputOrder
          stocks={this.state.stocks.map((stock) => {
            if (typeof stock === 'string') {
              return stock;
            } else {
              return stock.tiker;
            }
          })}
          sendOrder={this.sendOrder}
        />
        <Table responsive variant='dark'>
          <thead>
            <tr>
              <th>Order Type</th>
              <th>Ticker</th>
              <th>Initiator</th>
              <th>Shares</th>
              <th>Price</th>
              <th>Total value</th>
              <th>Filled</th>
            </tr>
          </thead>
          <tbody>{this.fillOrderTable()}</tbody>
        </Table>
      </div>
    );
  }
}
