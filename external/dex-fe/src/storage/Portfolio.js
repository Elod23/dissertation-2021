import React, { Component } from 'react';
import FormControl from 'react-bootstrap/FormControl';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import InputOrder from '../components/InputOrder';
import socketClient from '../io/socket';
import { Client } from '@stomp/stompjs';

const url = 'ws://localhost:15674/ws';

export default class Portfolio extends Component {
  constructor(props) {
    super(props);
    this.socket = socketClient();
    this.socket.on('stocks', (message) => {
      if (!message.error) {
        console.log('Stocks: ' + message);
        this.setState({ stocks: message });
      }
    });
    if (url) {
      this.stompClient = new Client({
        brokerURL: url,
        connectHeaders: {
          login: 'full_access',
          passcode: 'elod23',
          durable: 'true',
          'auto-delete': 'false',
        },
        reconnectDelay: 5000,
        heartbeatIncoming: 4000,
        heartbeatOutgoing: 4000,
      });
      this.stompClient.onConnect = (frame) => {
        this.subscription = this.stompClient.subscribe(
          '/queue/0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4stocks',
          this.handleOrder
        );
      };
      this.stompClient.activate();
    }
  }
  state = {
    stocks: [],
    newTicker: '',

    newStock: {
      ticker: '',
      initShareCount: 0,
      price: 0,
    },
  };
  socket;

  componentDidMount() {
    this._isMounted = true;
    this.socket.emit('stocks', this.props.address + 'stocks');
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  updateListingStock = (stock) => {
    console.log(stock.ticker + ' ' + stock.price + ' ' + stock.shares);
    this.setState({
      newStock: {
        ticker: this.state.newTicker,
        initShareCount: stock.shares,
        price: stock.price,
      },
    });
  };

  enlistStock = () => {
    console.log(this.state.newStock);
    this.socket.emit('newListing', this.state.newStock);
    this.setState({ newStock: {}, newTicker: '' });
  };

  handleTickerChange = (value) => {
    let stocks;
    if (this.stocks === undefined || this.stocks.length === 0) {
      stocks = [value];
    }
    this.setState({ newTicker: value, stocks: stocks });
  };

  render() {
    return (
      <div>
        <Container className='centered text-align-center'>
          <h1>Enlist stock</h1>

          <Row className='d-flex justify-content-center'>
            <FormControl
              aria-describedby='basic-addon1'
              value={this.state.newTicker}
              onChange={(e) => this.handleTickerChange(e.target.value)}
            />
            <br />
            <br />
            <br />
            <InputOrder
              stocks={this.state.stocks.map((stock) => {
                if (typeof stock === 'string') {
                  return stock;
                } else {
                  return stock.ticker;
                }
              })}
              sendOrder={this.updateListingStock}
            />{' '}
            <br />
            <Button
              onClick={
                this.enlistStock
              }>{`List ${this.state.newTicker}!`}</Button>
          </Row>
        </Container>
      </div>
    );
  }
}
