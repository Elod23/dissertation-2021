import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import TradingPanel from './TradingPanel';
import Join from '../components/chat/Join';
import Chat from '../components/Chat';

export default class Home extends Component {
  state = {
    jwt: '',
    address: '',
    joined: false,
    name: 'abi',
  };
  componentDidMount() {
    this._isMounted = true;
    this.setState({ jwt: this.props.jwt, address: this.props.address });
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  joinIn = (name) => {
    this.setState({ joined: true, name: name });
  };

  render() {
    return (
      <div>
        <Row className='fillRemaining'>
          <Container className='col-6'>
            <TradingPanel jwt={this.props.jwt} address={this.props.address} />
          </Container>
          <Container className='col-6'>
            {this.state.joined ? (
              <Chat name={this.state.name} room={'general'} />
            ) : (
              <Join jumpIn={this.joinIn} />
            )}
          </Container>
        </Row>
      </div>
    );
  }
}
