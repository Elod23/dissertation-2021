import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Badge from 'react-bootstrap/Badge';
import { Client } from '@stomp/stompjs';

import web3Client from '../io/web3';
import BaseCurrency from '../components/BSC';
import socketClient from '../io/socket';

const url = 'ws://localhost:15674/ws';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.socket = socketClient();
    this.web3 = web3Client();
    this.socket.on('watchlist', (message) => {
      if (!message.error) {
        console.log('Watchlist: ' + message);
        this.setState({ watchlist: message });
      }
    });

    this.socket.on('unsubscribed', (message) => {
      console.log(message);
      if (!message.error) {
        this.setState({ watchlist: message });
      }
    });
    if (url) {
      this.stompClient = new Client({
        brokerURL: url,
        connectHeaders: {
          login: 'full_access',
          passcode: 'elod23',
          durable: 'true',
          'auto-delete': 'false',
        },
        reconnectDelay: 5000,
        heartbeatIncoming: 4000,
        heartbeatOutgoing: 4000,
      });
      this.stompClient.onConnect = (frame) => {
        this.subscription = this.stompClient.subscribe(
          '/queue/data0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4',
          this.handleMarketData
        );
      };
      this.stompClient.activate();
    }
  }

  state = {
    bsc: 0,
    ether: 0,
    wei: 0,
    mint: 0,
    jwt: '',
    watchlist: [],
    trending: [],
    address: '0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4',
  };
  socket;
  stompClient;
  web3;
  // AMQPConnector = new AMQPConnector('amqp://localhost');

  subscriptionData = undefined;
  subscription;
  // ENDPOINT = 'http://localhost:8000/';

  componentDidMount = async () => {
    const bscBalance = await BaseCurrency.getBSCBalance(
      '0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4'
    );
    const balance = await this.web3.eth.getBalance(
      '0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4'
    );
    console.log(balance);
    const etherBalance = await this.web3.utils.fromWei(
      balance.toString(),
      'ether'
    );

    if (this.isOpen()) {
      this.socket.emit('listen', this.state.address);
    }

    this.setState({
      bsc: parseInt(bscBalance),
      jwt: this.props.jwt,
      wei: parseInt(balance),
      ether: parseFloat(etherBalance),
      watchlist: [],
      trending: ['HCMC', 'DPLS', 'AMAZ', 'GOOGL', 'AAPL'],
    });
  };

  componentDidUpdate = () => {
    console.log('address ' + this.state.address);
  };

  handleMarketData = (message) => {
    console.log(message.body);
    const marketData = JSON.parse(message.body);
    console.log(marketData);
    if (!marketData.error || !(marketData === {})) {
      this.setState({ watchlist: [...this.state.watchlist, marketData] });
    }
  };

  isOpen = () => {
    return this.socket.readyState === this.socket.OPEN;
  };

  setDesiredMintValue = (value) => {
    this.setState({ mint: value });
  };

  isMint = () => {
    return this.state.mint > 0;
  };

  mintBSC = () => {
    fetch('/api/investors/bsc/' + this.state.mint, {
      method: 'GET',
      headers: {
        Authorization: this.props.jwt,
      },
    }).then((res, err) => {
      if (!err) {
        if (res.status === 200) {
          this.setState({
            bsc: parseInt(this.state.bsc) + parseInt(this.state.mint),
            mint: 0,
          });
        }
      }
    });
  };

  unsubscribe = (ticker) => {
    alert('Stock: ' + ticker + 'removed!');
    const watches = this.state.watchlist.filter((stock) => stock !== ticker);
    const trends = this.state.trending.filter((stock) => stock !== ticker);

    console.log(this.state.address + ' ' + ticker);
    if (this.isOpen()) {
      this.socket.emit(
        'unsubscribe',
        { address: this.state.address, ticker: ticker },
        (err) => {
          console.log('I unsubscribed!');
          if (err) {
            alert(err);
          }
        }
      );
    }
  };

  subscribe = (tickerSymbol) => {
    let st = this.state.watchlist;
    if (st !== undefined) {
      st.push(tickerSymbol);
    } else {
      st = [tickerSymbol];
    }

    console.log(this.state.address + ' ' + tickerSymbol);
    if (this.isOpen()) {
      this.socket.emit(
        'subscribe',
        { iAddress: this.state.address, ticker: tickerSymbol },
        (err) => {
          if (err) {
            alert(err);
          }
        }
      );
    }
    this.setState({ watchlist: st });
  };

  returnBody = (list) => {
    console.log(list);
    return list !== undefined && list.length !== 0 ? (
      list.map((watch) => {
        return (
          <tr key={watch}>
            <td>{watch}</td>
            <td>0</td>
            <td>
              <Button
                className='btn btn-sm btn-danger'
                onClick={() => this.unsubscribe(watch)}>
                X
              </Button>
              {this.state.watchlist !== undefined &&
              this.state.watchlist.includes(watch) ? (
                <br />
              ) : (
                <Button
                  className='btn btn-sm btn-success'
                  onClick={() => this.subscribe(watch)}>
                  Add
                </Button>
              )}
            </td>
          </tr>
        );
      })
    ) : (
      <tr>
        <td>-</td>
        <td>0</td>
        <td>-</td>
      </tr>
    );
  };

  constructStockTable = (list) => {
    return (
      <div>
        <Table variant='dark'>
          <thead>
            <tr>
              <th>Ticker</th>
              <th>Price</th>
              <th>Unwatch</th>
            </tr>
          </thead>
          <tbody>{this.returnBody(list)}</tbody>
        </Table>
      </div>
    );
  };

  provideWatchList = () => {
    return this.constructStockTable(this.state.watchlist);
  };

  provideTrendingList = () => {
    return this.constructStockTable(this.state.trending);
  };

  fillBody = () => {
    return (
      <tbody>
        <tr>
          <td>1</td>
          <td>BSC</td>
          {this.state.bsc !== undefined ? (
            <td>{this.state.bsc}</td>
          ) : (
            <td>{undefined}</td>
          )}
          <td>
            <InputGroup className='sm'>
              <FormControl
                placeholder='Desired amount'
                aria-label='Amount (to the nearest dollar)'
                value={this.state.mint}
                onChange={(e) => this.setDesiredMintValue(e.target.value)}
              />
              <InputGroup.Append>
                <InputGroup.Text>BSC</InputGroup.Text>
                <InputGroup.Text>.00</InputGroup.Text>
                <Button
                  onClick={() => this.mintBSC()}
                  disabled={!this.isMint()}>
                  Mint!
                </Button>
              </InputGroup.Append>
            </InputGroup>
          </td>
        </tr>
        <tr>
          <td>1</td>
          <td>Ether</td>
          {this.state.ether !== undefined ? (
            <td>{this.state.ether}</td>
          ) : (
            <td>{undefined}</td>
          )}
          <td />
        </tr>
        <tr>
          <td>1</td>
          <td>Wei</td>
          {this.state.wei !== undefined ? (
            <td>{this.state.wei}</td>
          ) : (
            <td>{undefined}</td>
          )}
          <td />
        </tr>
      </tbody>
    );
  };

  render() {
    return (
      <Container>
        <Container>
          <b>Current allowance</b> <br />
          <Table variant='dark'>
            <thead>
              <tr>
                <th>#</th>
                <th>Currency</th>
                <th>Value</th>
                <th>Minting</th>
              </tr>
            </thead>
            {this.fillBody()}
          </Table>
        </Container>
        <Container className='container-fluid'>
          <Row>
            {' '}
            <Container className='col-6'>
              <h2>Watchlist</h2>
              {this.provideWatchList()}
            </Container>
            <Container className='col-6'>
              {' '}
              <h2>
                Trending stocks <Badge bg='danger'>New</Badge>
              </h2>
              {this.provideTrendingList()}
            </Container>
          </Row>
        </Container>
      </Container>
    );
  }
}
