import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { Link, withRouter } from 'react-router-dom';

class Register extends Component {
  state = {
    address: '',
    username: '',
    email: '',
    password: '',
  };

  validateForm = () => {
    return (
      this.state.email.length > 3 &&
      this.state.email.includes('@') &&
      this.state.password.length > 6 &&
      this.state.address.includes('0x') &&
      this.state.username.length >= 6
    );
  };

  setAddress = (addr) => {
    this.setState({ address: addr });
  };

  setEmail = (email) => {
    this.setState({ email: email });
  };

  setUsername = (username) => {
    this.setState({ username: username });
  };

  setPassword = (pass) => {
    this.setState({ password: pass });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { history } = this.props;
    const st = this.state;
    console.log(st);
    fetch('/api/investors/', {
      method: 'POST',
      cors: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(st),
    }).then((res, err) => {
      if (!err) {
        if (res.status === 201) {
          history.push('/login');
        } else {
          alert('Somethign went wrong, please try again!');
        }
      }
      console.log('Redirecting!');
    });
  };

  render() {
    return (
      <Container className='Login'>
        <Form onSubmit={() => this.handleSubmit()}>
          <Form.Group size='lg' controlId='email'>
            <Form.Label>Email</Form.Label>
            <Form.Control
              autoFocus
              type='email'
              value={this.state.email}
              onChange={(e) => this.setEmail(e.target.value)}
            />
          </Form.Group>
          <Form.Group size='lg' controlId='address'>
            <Form.Label>Address</Form.Label>
            <Form.Control
              autoFocus
              type='address'
              value={this.state.address}
              onChange={(e) => this.setAddress(e.target.value)}
            />
          </Form.Group>
          <Form.Group size='lg' controlId='username'>
            <Form.Label>User name</Form.Label>
            <Form.Control
              autoFocus
              type='username'
              value={this.state.username}
              onChange={(e) => this.setUsername(e.target.value)}
            />
          </Form.Group>
          <Form.Group size='lg' controlId='password'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type='password'
              value={this.state.password}
              onChange={(e) => this.setPassword(e.target.value)}
            />
          </Form.Group>
          <Container>
            <br />
            <br />
            <div className={'col text-center'}>
              <Button
                block
                size='lg'
                type='submit'
                disabled={!this.validateForm()}
                onClick={(e) => this.handleSubmit(e)}>
                Register
              </Button>
              <br />
              <br />
              <br />
              <br />
              <div>Already registered? {'  '}</div>
              <br />
              <Link className='btn btn-lg btn-info' to='/login'>
                Login here
              </Link>
            </div>
          </Container>
        </Form>
      </Container>
    );
  }
}
export default withRouter(Register);
