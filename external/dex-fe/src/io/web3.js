import Web3 from 'web3';

let web3;

const getWeb3 = () => {
  if (web3) {
    return web3;
  } else {
    web3 = new Web3('http://localhost:7545');
    return web3;
  }
};

export default getWeb3;
