import { Client } from '@stomp/stompjs';

let stompClient;
const getStompClient = () => {
  if (stompClient) {
    return stompClient;
  } else {
    stompClient = new Client({
      brokerUrl: 'ws://localhost:15674/ws',
      connectHeaders: {
        login: 'full_access',
        passcode: 'elod23',
        durable: 'true',
        'auto-delete': 'false',
      },
      debug: (str) => console.log(str),
      reconnectDelay: 5000,
      heartbeatIncoming: 4000,
      heartbeatOutgoing: 4000,
    });
    return stompClient;
  }
};

export default getStompClient;
