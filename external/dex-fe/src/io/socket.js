import io from 'socket.io-client';

let socket;

const getSocketClient = () => {
  if (socket) {
    return socket;
  } else {
    socket = io();
    return socket;
  }
};

export default getSocketClient;
