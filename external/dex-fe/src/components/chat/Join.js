import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';

import './style/Join.css';

const Join = (props) => {
  const [name, setName] = useState('');

  const handleJoin = () => {
    alert('You are joining the chat!');
    props.jumpIn(name);
  };

  return (
    <div className='joinOuterContainer'>
      <div className='joinInnerContainer'>
        <h1 className='heading'>Join</h1>
        <div>
          <input
            placeholder='Name'
            className='joinInput'
            type='text'
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <Button onClick={handleJoin}>Join</Button>
      </div>
    </div>
  );
};

export default Join;
