import Web3 from 'web3';
import bscInterface from '../contracts/BaseCurrency.json';
import Contract from 'web3-eth-contract';

const web3 = new Web3('http://localhost:7545');

const bscAbi = bscInterface.abi;
const bscAddress = '0x2021F3586C6AC641A1E0e2c7F610bEf6Dc8d37ca';

Contract.setProvider(web3.currentProvider);

const BaseCurrency = new Contract(bscAbi, bscAddress);

BaseCurrency.getBSCBalance = (address) => {
  return BaseCurrency.methods
    .balanceOf(address)
    .call()
    .then((res) => {
      return res;
    });
};

export default BaseCurrency;
