import React, { useState, useEffect } from 'react';
import socketClient from '../io/socket';

import TextContainer from './chat/TextContainer';
import Messages from './chat/Messages';
import InfoBar from './chat/InfoBar';
import Input from './chat/Input';

import './chat/style/Chat.css';

let socket;

const Chat = (props) => {
  const [room, setRoom] = useState('');
  const [name, setName] = useState('');
  const [users, setUsers] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);
  const [flag, setFlag] = useState(0);
  socket = socketClient();

  useEffect(() => {
    const stock = props.room;
    const name = props.name;

    setRoom(stock);
    setName(name);

    socket.emit('join', { name, room }, (error) => {
      if (error) {
        setFlag(1);
        alert(error);
      }
    });
  }, [name, room, props.name, props.room]);

  useEffect(() => {
    socket.on('message', (message) => {
      setMessages((messages) => [...messages, message]);
    });

    socket.on('roomData', ({ users }) => {
      setUsers(users);
    });
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();

    if (message) {
      socket.emit('sendMessage', message, () => setMessage(''));
    }
  };

  if (flag) {
    window.location.reload();
  }

  return (
    <div className='outerContainer'>
      <div className='chatcontainer'>
        <InfoBar room={room} />
        <Messages messages={messages} name={name} />
        <Input
          message={message}
          setMessage={setMessage}
          sendMessage={sendMessage}
        />
      </div>
      <TextContainer users={users} />
    </div>
  );
};
export default Chat;
