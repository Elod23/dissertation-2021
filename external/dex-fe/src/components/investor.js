import Web3 from 'web3';
import investorInterface from '../contracts/Investor.json';
import Contract from 'web3-eth-contract';

const web3 = new Web3('http://localhost:7545');

const investorAbi = investorInterface.abi;
const investorAddress = '0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4';
const brokerAddress = '0x42B743E2CA65673F5B878889d3767f12F7C00b1D';
const donorAddress = '0xc58453178A32f431646ebCf8e433a4636A125C4D';
Contract.setProvider(web3.currentProvider);

const Investor = new Contract(investorAbi, investorAddress);

Investor.enlistAtBroker = (brokerName = 'Broker') => {
  return Investor.methods
    .enlistAtBroker(brokerAddress, brokerName)
    .send({ from: donorAddress, gas: 6721975 })
    .then((res) => {
      return res;
    });
};

export default Investor;
