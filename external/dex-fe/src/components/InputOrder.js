import React, { useState, useEffect } from 'react';

import InputGroup from 'react-bootstrap/InputGroup';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

const InputOrder = (props) => {
  const [stocks, setStocks] = useState([]);
  const [ticker, setTicker] = useState('');
  const [amount, setAmount] = useState(0);
  const [price, setPrice] = useState(0);
  const [orderType, setType] = useState('');

  const stocksFromParent = props.stocks;
  const orderTypes = ['LONG', 'SELL', 'SHORT'];

  useEffect(() => {
    setStocks(stocksFromParent);
  }, [stocksFromParent]);

  const validateOrder = () => {
    return ticker !== '' && amount !== 0 && price !== 0 && orderType !== '';
  };

  const fillTickerDropdown = () => {
    return stocks.map((stock) => (
      <Dropdown.Item eventKey={stock}>{stock}</Dropdown.Item>
    ));
  };

  const fillTypeDropdown = () => {
    return orderTypes.map((type) => (
      <Dropdown.Item eventKey={type}>{type}</Dropdown.Item>
    ));
  };

  const handleAmountChange = (value) => {
    setAmount(value);
  };

  const handlePriceChange = (value) => {
    setPrice(value);
  };

  const handleTickerChange = (value) => {
    setTicker(value);
  };

  const handleTypeChange = (value) => {
    setType(value);
  };

  const sendOrder = () => {
    const order = {
      ticker: ticker,
      type: orderType,
      price: price,
      shares: amount,
    };
    props.sendOrder(order);
  };

  return (
    <div>
      <InputGroup className='mb-3'>
        <DropdownButton
          as={InputGroup.Prepend}
          variant='outline-secondary'
          title={ticker ? ticker : 'Ticker'}
          value={ticker}
          onSelect={(e) => handleTickerChange(e)}
          id='input-group-dropdown-1'>
          {fillTickerDropdown()}
        </DropdownButton>
        <FormControl
          aria-describedby='basic-addon1'
          value={amount}
          onChange={(e) => handleAmountChange(e.target.value)}
        />
        <DropdownButton
          as={InputGroup.Prepend}
          variant='outline-secondary'
          title={orderType ? orderType : 'OrderType'}
          value={orderType}
          id='input-group-dropdown-1'
          onSelect={(e) => handleTypeChange(e)}>
          {fillTypeDropdown()}
        </DropdownButton>
        <FormControl
          placeholder='0'
          aria-label='Amount (to the nearest dollar)'
          value={price}
          onChange={(e) => handlePriceChange(e.target.value)}
        />
        <InputGroup.Text>BSC</InputGroup.Text>
        <Button onClick={() => sendOrder()} disabled={!validateOrder()}>
          Send
        </Button>
      </InputGroup>
    </div>
  );
};

export default InputOrder;
