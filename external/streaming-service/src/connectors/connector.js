import Web3 from 'web3';

const web3 = new Web3(Web3.givenProvider || process.env.GANACHE_ADDRESS || 'ws://localhost:7545');
export default web3;
