import web3 from './connector';
import BaseCurrencyDef from '../../contracts/BaseCurrency.json';
import Contract from 'web3-eth-contract';

const BSCprice = process.env.BSC_TO_WEI || 240000000000000;
const baseCurrencyABI = BaseCurrencyDef.abi;
const baseCurrencyAddress =
  process.env.BASECURRENCY_ADDRESS || '0x2021F3586C6AC641A1E0e2c7F610bEf6Dc8d37ca';
const donorAddress = process.env.TEST_DONOR_ADDRESS || '0xc58453178A32f431646ebCf8e433a4636A125C4D';
Contract.setProvider(web3.currentProvider);

const BaseCurrency = new Contract(baseCurrencyABI, baseCurrencyAddress);

BaseCurrency.listenEvents = () => {
  const eventsOfContract = web3.utils._.find(
    BaseCurrency._jsonInterface,
    (e) => e.type === 'event'
  );

  web3.eth
    .subscribe('logs', {
      address: baseCurrencyAddress,
      topics: [eventsOfContract.signature],
    })
    .then((err, event) => {
      if (!err) {
        const eventObj = web3.eth.abi.decodeLog(
          eventsOfContract.inputs,
          event.data,
          event.topics.slice(1)
        );
        console.log(`New event: ${eventName}`, eventObj);
      }
    });
};

BaseCurrency.enlistStock = (stock) => {
  return BaseCurrency.methods
    .enlistStock(
      stock.ticker,
      stock.shares,
      stock.price,
      stock.brokerAddress,
      stock.enlisterAddress
    )
    .send({
      from: donorAddress,
      value: (stock.price * stock.shares * BSCprice) / 100,
      gas: 6721975,
    })
    .then((err, hash) => {
      if (err) {
        return false;
      } else {
        return hash;
      }
    });
};

export default BaseCurrency;
