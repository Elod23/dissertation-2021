import web3 from './connector';
import InvestorDef from '../../contracts/Investor.json';
import Contract from 'web3-eth-contract';

const BSCprice = process.env.BSC_TO_WEI || 240000000000000;
const investorAbi = InvestorDef.abi;
const investorAddress =
  process.env.INVESTOR_ADDRESS || '0xe0C2A1F48890e55e7Ee33d7f8f832Be1dfDACFD4';
const bscAddress = process.env.BASECURRENCY_ADDRESS || '0x2021F3586C6AC641A1E0e2c7F610bEf6Dc8d37ca';
const donorAddress = process.env.TEST_DONOR_ADDRESS || '0xc58453178A32f431646ebCf8e433a4636A125C4D';
const brokerAddress = process.env.BROKER_ADDRESS || '0x42B743E2CA65673F5B878889d3767f12F7C00b1D';
Contract.setProvider(web3.currentProvider);

const Investor = new Contract(investorAbi, investorAddress);

Investor.mintBSC = async (amount) => {
  return Investor.methods
    .obtainBSC(bscAddress, amount)
    .send({ from: donorAddress, value: amount * BSCprice, gas: 6721975 })
    .then((res, err) => {
      if (!err) {
        return res;
      } else {
        throw err;
      }
    })
    .catch((err) => console.log(err));
};

Investor.getPortfolio = async () => {
  return await Investor.methods.getOwnedTickers().call({ from: donorAddress, gas: 6721975 });
};

Investor.sendOrder = async (order) => {
  switch (order.type) {
    case 'LONG':
      return Investor.methods
        .long(order.ticker, order.amount, order.price, brokerAddress, bscAddress)
        .send({ from: donorAddress, gas: 6721975 })
        .then((res, err) => {
          if (!err) {
            console.log(res);
            return res;
          }
        })
        .catch((err) => {
          return err;
        });

    case 'SELL':
      return Investor.methods
        .sell(order.ticker, order.amount, order.price, brokerAddress)
        .send({ from: donorAddress, gas: 6721975 })
        .then((res, err) => {
          if (!err) {
            console.log(res);
            return res;
          }
        })
        .catch((err) => {
          return err;
        });

    case 'LONG':
      return Investor.methods
        .long(order.ticker, order.amount, order.price, brokerAddress)
        .send({ from: donorAddress, gas: 6721975 })
        .then((res, err) => {
          if (!err) {
            console.log(res);
            return res;
          }
        })
        .catch((err) => {
          return err;
        });

    case 'CLOSE_SHORT':
      return Investor.methods
        .long(order.ticker, order.amount, order.price, brokerAddress, bscAddress)
        .send({ from: donorAddress, gas: 6721975 })
        .then((res, err) => {
          if (!err) {
            console.log(res);
            return res;
          }
        })
        .catch((err) => {
          return err;
        });
  }
};

export default Investor;
