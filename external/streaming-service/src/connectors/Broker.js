import web3 from './connector';
import BrokerDef from '../../contracts/Broker.json';
import Contract from 'web3-eth-contract';
import OrderDao from '../dao/OrderDao';
import investorDao from '../dao/InvestorDao';

const BSCprice = process.env.BSC_TO_WEI || 240000000000000;
const brokerAbi = BrokerDef.abi;
const brokerAddress = process.env.BROKER_ADDRESS || '0x42B743E2CA65673F5B878889d3767f12F7C00b1D';
const donorAddress = process.env.TEST_DONOR_ADDRESS || '0xc58453178A32f431646ebCf8e433a4636A125C4D';

Contract.setProvider(web3.currentProvider);

const Broker = new Contract(brokerAbi, brokerAddress);

Broker.listenEvents = () => {
  const eventsOfContract = web3.utils._.find(Broker._jsonInterface, (e) => e.type === 'event');

  web3.eth
    .subscribe('logs', {
      address: baseCurrencyAddress,
      topics: [eventsOfContract.signature],
    })
    .then((err, event) => {
      if (!err) {
        const eventObj = web3.eth.abi.decodeLog(
          eventsOfContract.inputs,
          event.data,
          event.topics.slice(1)
        );
        console.log(`New event: ${eventName}`, eventObj);
      }
    });
};

Broker.getOrdersOfShareholder = (address) => {
  return Broker.methods
    .getOrdersForShareholder(address)
    .call({ from: donorAddress, gas: 6721975 })
    .then(async (res, err) => {
      if (!err) {
        console.log(res);
        if (res && res.length == 0) {
          return [];
        }
        const investor = await investorDao.findByAddress(address);
        const orders = res.map((order) => {
          return {
            externalOrderId: order.orderId,
            initiator: investor._id,
            timestamp: order.time,
            type: order.typeOfOrder,
            ticker: order.tickerSymbol,
            price: order.price,
            shares: order.amountOfShares,
            filled: order.isFilled || false,
          };
        });
        if (orders) {
          orders.forEach((order) => {
            console.log(order);
            OrderDao.insertOrder(order).then((res) => console.log('saved ' + res._id));
          });
        }
        return orders;
      }
    });
};

Broker.getMarketBalanceOfTicker = (ticker) => {
  console.error(ticker);
  return Broker.methods
    .getMarketBalanceForSecurity(ticker)
    .call({ from: donorAddress, gas: 6721975 })
    .then((res, err) => {
      if (!err) {
        return {
          supply: res[0],
          demand: res[1],
          borrowed: res[2],
          maxBid: res[3],
          minAsk: res[4],
        };
      }
    })
    .catch((err) => {
      return {};
    });
};

export default Broker;
