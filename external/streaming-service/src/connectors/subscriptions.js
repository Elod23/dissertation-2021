import web3 from './connector';

var investorSubscription = web3.eth
  .subscribe('logs', {
    address: process.env.INVESTOR_ADDRESS,
  })
  .then((err, res) => {
    if (res) {
      return res;
    }
    if (err) {
      console.log(err);
    }
  });
