import express from 'express';
import fetch from 'node-fetch';
import investorDao from '../dao/InvestorDao';
import validateBody from '../middleware/validateRequestBody';
import Investor from '../connectors/Investor';
import producer from '../producer/produceOperations';
import bcrypt from 'bcryptjs';

const router = express.Router();

//  findByIdentifier
router.get('/:identifier', (req, res) => {
  console.log(req.params.identifier);
  const identifier = req.params.identifier;
  console.log(`Queried inestor by id ${identifier}`);
  investorDao
    .findById(identifier)
    .then((investor) =>
      investor
        ? res.json(investor)
        : res.status(404).json({ message: `Investor with id ${identifier} not found.` })
    )
    .catch((err) =>
      res.status(500).json({
        message: `An exception occurred while searching with filter: ${identifier} => ${err.message}`,
      })
    );
});

router.get('/', (req, res) => {
  investorDao
    .findAllInvestors()
    .then((investors) => res.json(investors))
    .catch((err) =>
      res.status(500).json({
        message: `An error occurred during findAll: ${err.message}`,
      })
    );
});

router.get('/bsc/:amount', async (req, res) => {
  const { amount } = req.params;
  console.log(amount);
  const resultProm = await Investor.mintBSC(amount);
  if (resultProm) {
    res.sendStatus(200);
  } else {
    res.sendStatus(500);
  }
});

router.post('/credentials', async (req, res) => {
  const email = req.body.email;
  console.log(email);
  const reqPw = req.body.password;
  const investor = await investorDao.findByEmail(email);

  const credentials = {
    email: investor.email,
    password: investor.password,
  };

  const pwMatches = await bcrypt.compare(reqPw, investor.password);
  if (investor) {
    console.log('Second level');
    if (pwMatches) {
      console.log('Third level');
      if (investor.jwt) {
        console.log('Fourth level');
        const valid = await fetch('http://localhost:8081/auth/', {
          method: 'GET',
          mode: 'cors',
          headers: {
            Authorization: investor.jwt,
          },
        });
        console.log('Had jwt level');
        const value = await valid.text();
        if (value) {
          console.log('Response from spring: ' + value);
          return res.status(200).json({ message: 'Bearer ' + investor.jwt });
        } else {
          const authRes = await fetch('http://localhost:8081/auth/', {
            method: 'POST',
            mode: 'cors',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(credentials),
          });
          const jwt = await authRes.text();
          console.log('token: ' + jwt);
          investorDao
            .updateInvestor({ email: email }, { jwt: jwt }, { upsert: false })
            .then((investor) => {
              const changes = {
                type: 'MODIFY',
                changes: investor,
              };
              producer.sendDiagnostics(changes, email);
            })
            .catch((err) =>
              res.status(500).json({
                message: `An exception occurred during update : ${err.message}`,
              })
            );
          return res.status(200).json({ message: 'Bearer ' + jwt });
        }
      } else {
        console.log('Sending creds for jwt');
        const creds = {
          email: investor.email,
          address: investor.address,
          password: investor.password,
        };
        const authRes = await fetch('http://localhost:8081/auth/', {
          method: 'POST',
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(creds),
        });
        const jwt = await authRes.text();
        console.log('else token: ' + jwt);
        console.log('email ' + email);
        investorDao
          .updateInvestor({ email: email }, { jwt: jwt }, { upsert: false })
          .then((investor) => {
            console.log('Investor found!');
            console.log(investor);
            const changes = {
              type: 'MODIFY',
              changes: investor,
            };
            producer.sendDiagnostics(changes, email);
          })
          .catch((err) =>
            res.status(500).json({
              message: `An exception occurred during update : ${err.message}`,
            })
          );
        return res.status(200).json({ message: 'Bearer ' + jwt });
      }
    } else {
      res.status(403).json({ message: 'Invalid password!' });
    }
  } else {
    res.status(400).json({ message: 'Invalid credentials!' });
  }
});

router.post('/:address/credentials', async (req, res) => {
  const { address } = req.params;
  const investor = await investorDao.findByAddress(address);
  if (!req.get('Authorization') && !investor.jwt) {
    const credentials = {
      email: investor.email,
      password: investor.password,
      address: investor.address,
    };

    const authRes = await fetch('http://auth:8081/auth/', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(credentials),
    });
    const jwt = await authRes.text();
    console.log(jwt);
    if (jwt) {
      investor.jwt = jwt;
      const changes = await investorDao
        .updateInvestor({ address: investor.address }, investor, {
          upsert: false,
        })
        .then((err, res) => {
          if (err) {
            res.status(500);
          } else {
            res.status(200).json({ message: 'Bearer ' + jwt });
          }
        });
    }
  } else {
    let token;
    if (investor && investor.jwt) {
      token = investor.jwt;
    } else {
      token = req.get('Authorization');
    }
    console.log(token);
    const valid = await fetch('http://auth:8081/auth/', {
      method: 'GET',
      mode: 'cors',
      headers: {
        Authorization: token,
      },
    });
    const value = await valid.text();
    console.log(value);
    if (value === 'true') {
      res.status(200).json({ message: 'Token still valid' });
    } else {
      res.status(403).json({ message: 'Invalid token' });
    }
  }
});

// insert
router.post('/', validateBody(['address', 'username', 'email', 'password']), async (req, res) => {
  const request = req.body;
  console.log(request.password);
  const hashedPassword = await bcrypt.hash(request.password, 8);
  request.password = hashedPassword;
  console.log(request);
  investorDao
    .insertInvestor(request)
    .then((investor) => {
      const changes = {
        type: 'INSERT',
        changes: investor,
      };
      producer.sendDiagnostics(changes, req.body.address);
      res.status(201).location(`${req.fullUrl}/${investor.address}`).json(investor);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        message: `An exception occurred during investor creation: ${req.body} => ${err.message}`,
      });
    });
});

// put upsert
router.put('/:identifier', async (req, res) => {
  const { identifier } = req.params;
  await investorDao
    .updateInvestor({ _id: identifier }, req.body, { upsert: true })
    .then((investor) => {
      const changes = {
        type: 'MODIFY',
        changes: investor,
      };
      producer.sendDiagnostics(changes, identifier);
      investor
        ? res.sendStatus(204)
        : res.status(404).json({ message: 'An exception occurred during update!' });
    })
    .catch((err) =>
      res.status(500).json({
        message: `An exception occurred during update : ${err.message}`,
      })
    );
});

// patch update
router.patch('/:identifier', (req, res) => {
  const { identifier } = req.params;
  investorDao
    .updateInvestor({ _id: identifier }, req.body, { upsert: false })
    .then(async (investor) => {
      const changes = {
        type: 'MODIFY',
        changes: req.body,
      };
      producer.sendDiagnostics(changes, identifier);

      investor
        ? res.sendStatus(204)
        : res.status(404).json({ message: 'An exception occurred during update!' });
    })
    .catch((err) =>
      res.status(500).json({
        message: `An exception occurred during update : ${JSON.stringify(err.message)}`,
      })
    );
});

// delete
router.delete('/:address', (req, res) => {
  const { address } = req.params;
  investorDao
    .deleteInvestor({ address: address })
    .then((result) => {
      const changes = {
        type: 'DELETE',
        changes: result,
      };
      producer.sendDiagnostics(changes, address);
      result.deletedCount > 0
        ? res.sendStatus(204)
        : res.status(404).json({
            message: `No investor found with ${JSON.stringify(address)} address`,
          });
    })
    .catch((err) =>
      res.status(500).json({
        message: `An exception occurred during delete : ${JSON.stringify(err.message)}`,
      })
    );
});

module.exports = router;
