// this index file will be the root of the exposed REST APIs
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import hateoasReq from '../middleware/hateoas';
import investorRoutes from './investor';
import contractRoutes from './contracts';

const router = express.Router();

// short logs
router.use(morgan('tiny'));
// CORS headers
router.use(cors());
// engaging HATEOAS principles
router.use(hateoasReq);
// JSON parsing
router.use(bodyParser.json());
// All respective API endpoints from root down
router.use('/investors', investorRoutes);
router.use('/contracts', contractRoutes);

module.exports = router;
