import express, { Router } from 'express';
import BaseCurrency from '../connectors/BaseCurrency';

const router = express.Router();

router.post('/bsc/stock', (req, res) => {
  const { stock } = req.body;
  const requiredInformation =
    stock.ticker && stock.shares && stock.price && stock.brokerAddres && stock.enlisterAddres;
  if (requiredInformation) {
    BaseCurrency.enlistStock(stock).then((err, result) => {
      if (!err) {
        res.send(201);
      } else {
        res.send(500).json({ message: 'Oops! Something went wrong, please try again later!' });
      }
    });
  } else {
    res.send(400).json({ message: 'One or more attributes of the body are missing!' });
  }
});

router.get('/stock/', (req, res) => {
  console.log('initialize stock sync!');
});

module.exports = router;
