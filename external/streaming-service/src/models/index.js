import 'dotenv/config';

import Investor from './Investor';
import Transaction from './Transaction';
import dbConn from '../dao/dbConn';
import Order from './Order';
import Stock from './Stock';

const models = { Investor, Order, Transaction, Stock };

module.exports = {
  ...models,
  dbConn,
};
