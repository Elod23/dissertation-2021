const mongoose = require('mongoose');

const mongoSchema = mongoose.Schema;
const autoIncrement = require('mongoose-sequence')(mongoose.connection);

const orderSchema = mongoose.Schema({
  orderId: {
    type: Number,
  },
  externalOrderId: {
    type: Number,
  },
  initiator: {
    type: mongoSchema.Types.ObjectId,
    ref: 'Investor',
  },
  ticker: {
    type: String,
  },
  type: {
    type: String,
  },
  price: {
    type: Number,
  },
  shares: {
    type: Number,
  },
  filled: {
    type: Boolean,
    default: false,
    required: true,
  },
  timestamp: {
    type: Date,
    default: Date.now,
    required: true,
  },
});
orderSchema.plugin(autoIncrement, { inc_field: 'orderId' });
const Order = mongoose.model('Order', orderSchema);
export default Order;
