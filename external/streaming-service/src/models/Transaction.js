import mongoose from 'mongoose';

const transactionSchema = new mongoose.Schema({
  id: {
    type: Number,
    unique: true,
  },
  from: {
    type: String,
  },
  to: {
    type: String,
  },
  value: {
    type: Number,
  },
});

const Transaction = mongoose.model('Transaction', transactionSchema);

export default Transaction;
