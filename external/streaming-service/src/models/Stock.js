const mongoose = require('mongoose');

const mongoSchema = mongoose.Schema;
const autoIncrement = require('mongoose-sequence')(mongoose.connection);

const stockSchema = mongoose.Schema({
  id: {
    type: Number,
  },
  ticker: {
    type: String,
    unique: true,
  },
  borrowable: {
    type: Number,
  },
  price: {
    type: Number,
  },
  additionalOfferingsAllowed: {
    type: Boolean,
  },
  creationTimestamp: {
    type: Date,
    default: Date.now,
    required: true,
  },
  orders: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Order' }],
});
stockSchema.plugin(autoIncrement, { inc_field: 'id' });
const Stock = mongoose.model('Stock', stockSchema);
export default Stock;
