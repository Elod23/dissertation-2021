import mongoose from 'mongoose';

const investorSchema = new mongoose.Schema({
  address: {
    type: String,
    unique: true,
  },
  username: {
    type: String,
    unique: true,
  },
  email: {
    type: String,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  funds: {
    type: Number,
  },
  jwt: {
    type: String,
  },
  watchlist: [
    {
      type: String,
    },
  ],
});

// this helper method will be used to "safe delete"
// all orders if an investor request for deletion arrives
investorSchema.pre('remove', (next) => {
  this.model('Order').deleteMany({ investor: this._id }, next);
});

const Investor = mongoose.model('Investor', investorSchema);

export default Investor;
