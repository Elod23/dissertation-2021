import amqp from 'amqplib';
import callback_api from 'amqplib/callback_api';

export default class AMQPConnector {
  constructor(connectionURL) {
    this.connectionUrl = connectionURL;
    this.connection = {};
  }

  async sendMessage(exchangeName, topicKey, message) {
    this.connection = await amqp.connect();

    console.log('connection: ' + this.connection);
    if (this.connection) {
      const channel = await this.connection.createChannel();
      // await channel.assertExchange(exchangeName, 'topic', { durable: false });
      await channel.assertQueue(topicKey);
      await channel.bindQueue(topicKey, exchangeName, topicKey);
      channel.publish(exchangeName, topicKey, Buffer.from(JSON.stringify(message)));
      console.log('Message sent to: ' + exchangeName);
    }
  }

  decodeMessage(msg) {
    console.log('Message: ' + msg);
    return JSON.parse(msg.content.toString());
  }

  async startConsumer(exchangeName, topicKey, queue, handleConsume = this.decodeMessage) {
    this.connection = callback_api.connect();
    if (!this.connection) {
      throw new Error('Connecting to RabbitMQ not possible!');
    }
    const channel = await this.connection.createChannel();
    await channel.assertExchange(exchangeName, 'topic', { durable: true });
    await channel.assertQueue(queue, { durable: true });
    console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', queue);
    channel.consume(queue, handleConsume);
  }
}
