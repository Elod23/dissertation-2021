/* eslint-disable import/no-unresolved */
import amqp from 'amqplib';

exports.sendDiagnostics = async (changes, user) => {
  const connection = await amqp.connect();
  const channel = await connection.createChannel();
  const exchange = 'topic_operations';
  const key = `operation.${changes.type}`;
  const message = {
    user,
    time: new Date(),
    changes: { changes },
  };
  await channel.assertExchange(exchange, 'topic', { durable: false });
  await channel.assertQueue(key, { durable: true });
  await channel.bindQueue(key, exchange, key);
  channel.publish(exchange, key, Buffer.from(JSON.stringify(message)));
  console.log(`Backend sent msg: ${message} with key: ${key}`);
};

exports.sendBookReturnedUpdate = async (book) => {
  const connection = await amqp.connect();
  const channel = await connection.createChannel();
  const exchange = 'topic_books';
  const message = {
    time: new Date(),
    book,
  };
  await channel.assertExchange(exchange, 'fanout', { durable: false });
  await channel.assertQueue(book.title, { durable: false });
  await channel.bindQueue(book.title, exchange, '');
  channel.publish(exchange, '', Buffer.from(JSON.stringify(message)));
  console.log(
    `Backend sent msg: ${message} to exhange: ${exchange} and created queue named: ${book.title}`
  );
};

// exports.creatPersonalizedQueue = async (user) => {
//   const connection = await amqp.connect();
//   const channel = await connection.createChannel();
//   const exchange = 'topic_books';

//   await channel.assertExchange(exchange, 'fanout', { durable: false });
//   await channel.assertQueue(
//     '',
//     {
//       exclusive: true,
//     },
//     (err, q) => {
//       console.log(`User ${user} is waiting on queue ${q.queue}`);
//       channel.bindQueue(q.queue, exchange, '');
//       return q;
//     }
//   );
// };
