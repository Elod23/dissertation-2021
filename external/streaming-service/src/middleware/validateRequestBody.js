// accepts an array of prop names
//  checks whether the request body has these props
// required for eg. inserts
export default function hasProps(propNames) {
  return (req, res, next) => {
    const nonExistentProps = propNames.filter((propName) => !(propName in req.body));
    if (nonExistentProps.length === 0) {
      next();
    } else {
      console.log('Here');
      res
        .status(400)
        .json({ message: `The following fields are missing: ${nonExistentProps.join(', ')}` });
    }
  };
}
