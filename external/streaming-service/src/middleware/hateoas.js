const url = require('url');

// the middleware is used to create the HATEOAS
// response structure
module.exports = (req, res, next) => {
  req.fullUrl = url.format({
    protocol: req.protocol,
    host: req.get('host'),
    pathname: req.originalUrl,
  });
  next();
};
