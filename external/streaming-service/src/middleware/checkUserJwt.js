export default function hasJwt() {
  return (req, res, next) => {
    if (req.headers.authorization) {
      fetch('http://localhost:8081/auth/mail', {
        method: 'GET',
        mode: 'cors',
        headers: {
          Authorization: req.headers.authorization,
        },
        body: {
          mail: req.body,
        },
      }).then((res, err) => {
        if (res) {
          next();
        } else {
          res.status(403).send();
        }
      });
    } else {
      res.status(403).send();
    }
  };
}
