export default function hasJwt() {
  return (req, res, next) => {
    console.log('Here ' + req.url);
    if (req.url.includes('auth') || (req.url.includes('investor') && req.method === 'POST')) {
      next();
    } else {
      if (req.headers.authorization) {
        next();
      } else {
        res.status(401).send();
      }
    }
  };
}
