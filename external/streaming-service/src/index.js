import cors from 'cors';
import 'dotenv/config';
import models from './models';
import apiRoutes from './api';
import validateJwt from './middleware/validateJwt';
import socketIo from 'socket.io';
import http from 'http';
import AMQPConnector from './producer/amqpConnector';

// stock & investor deps
import investorDao from './dao/InvestorDao';
import stockDao from './dao/StockDao';
import orderDao from './dao/orderDao';
import Broker from './connectors/Broker';
import BaseCurrency from './connectors/BaseCurrency';

// chat deps
import Chat from './chat/chat';
import Investor from './connectors/Investor';

const express = require('express');

const app = express();
const server = http.createServer(app);
const io = socketIo().listen(server, {
  cors: {
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
  },
});
const amqp = new AMQPConnector('amqp://localhost');

app.use(cors({ credentials: true }));
app.use(validateJwt());
app.options('*', cors());
app.use('/api', apiRoutes);
// app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let interval;
io.on('connection', (socket) => {
  //First we handle stock data, then chat
  socket.on('listen', async (message, cb) => {
    console.log('listen ' + message);
    const investor = await investorDao.findByAddress(message);

    if (investor) {
      const watchlist = investor.watchlist;
      socket.emit('watchlist', watchlist);
      watchlist.forEach((ticker) => {
        Broker.getMarketBalanceOfTicker(ticker)
          .then((res, err) => {
            if (!err) {
              console.log(res);
              try {
                amqp.sendMessage('investor', 'data' + message, JSON.stringify(res));
              } catch (err) {
                console.log(err);
              }
            }
          })
          .catch((err) => {
            console.log(err);
            try {
              amqp.sendMessage(
                'investor',
                'data' + message,
                JSON.stringify({ ticker: ticker, error: 'No market data' })
              );
            } catch (err) {
              console.log.log(err);
            }
          });
      });
    } else {
      socket.emit('watchlist', { error: '400' });
    }
  });

  socket.on('stocks', (message) => {
    console.log('sending stocks');
    stockDao
      .findAllStocks()
      .then((res, err) => {
        if (!err) {
          socket.emit('stocks', res);
        } else {
          console.log(err);
        }
      })
      .catch((err) => console.log(err));
  });

  socket.on('newListing', async (message) => {
    console.log('Trying to list: ' + message.ticker);

    const exStock = {
      ticker: message.ticker,
      shares: message.initShareCount,
      price: message.price,
      brokerAddress: '0x42B743E2CA65673F5B878889d3767f12F7C00b1D',
      enlisterAddress: '0xe0c2a1f48890e55e7ee33d7f8f832be1dfdacfd4',
    };
    BaseCurrency.enlistStock(exStock)
      .then((res, err) => {
        if (!err) {
          console.log(res);
          const stock = {
            ticker: message.ticker,
            borrowable: message.initShareCount,
            price: message.price,
            additionalOfferingsAllowed: true,
          };
          stockDao
            .insertStock(stock)
            .then((res, err) => {
              if (!err) {
                stockDao
                  .findAllStocks()
                  .then((res, err) => {
                    if (!err) {
                      socket.emit('stocks', res);
                    }
                  })
                  .catch((err) => console.log(err));
              }
            })
            .catch((err) => console.log(err));
        }
      })
      .catch((err) => console.log(err));
  });

  socket.on('newOrder', async (message) => {
    console.log('Orders for: ' + message);
    const investor = await investorDao.findByAddress(message.initiator);
    message.initiator = investor._id;
    orderDao.insertOrder(message).then((res, err) => {
      if (!err) {
        console.log('emitted OK');
        socket.emit('OK');
        Investor.sendOrder(message)
          .then((res, err) => {
            if (!err) {
              console.log('From blockchain: ' + res);
              try {
                amqp.sendMessage('investor', message.initiator, JSON.stringify(res));
              } catch (err) {
                console.log.log(err);
              }
            }
          })
          .catch((err) => console.log(err));
      }
    });
  });

  socket.on('needCredentials', async (message) => {
    console.log(message);
    const investorCreds = await investorDao.findByAddress(message);
    if (investorCreds) {
      socket.emit('credentials', investorCreds);
    }
  });

  socket.on('needOrders', async (msg) => {
    const message = msg;
    const address = message.replace('orders', '');
    console.log('message ' + message);
    console.log('message ' + address);
    console.log('Orders for: ' + address);
    Broker.getOrdersOfShareholder(address)
      .then((res, err) => {
        if (!err) {
          console.log(res);
          res.forEach((res) => {
            try {
              amqp.sendMessage('investor', message + 'orders', res);
            } catch (err) {
              console.log(err);
            }
          });
        } else {
          console.log(err);
        }
      })
      .catch((err) => console.log(err));
  });

  socket.on('subscribe', async (message, cb) => {
    console.log('subscribe ' + message.ticker);
    const investor = await investorDao.findByAddress(message.iAddress);
    if (investor) {
      const updatedInvestor = await investorDao.addToWatchList(
        { address: message.iAddress },
        message.ticker,
        {
          upsert: false,
        }
      );
      if (updatedInvestor) {
        socket.emit('watchlist', updatedInvestor.watchlist);
      } else {
        socket.emit('watchlist', { error: '400' });
      }
    } else {
      socket.emit('watchlist', { error: '400' });
    }
  });
  socket.on('unsubscribe', async (message, callback) => {
    console.log('unsubscribe' + message.ticker);
    await investorDao.removeFromWatchList({ address: message.address }, message.ticker, {
      upsert: false,
    });
    const newData = await investorDao.findByAddress(message.address);
    console.log(newData);
    console.log('send unsub: ' + newData && !newData.watchlist.includes(message.ticker));
    if (newData && !newData.watchlist.includes(message.ticker)) {
      socket.emit('unsubscribed', newData.watchlist);
    } else {
      socket.emit('unsubscribed', JSON.stringify({ error: '500' }));
    }
  });

  socket.on('join', ({ name, room }, callback) => {
    console.log('name: ' + name + ' room: ' + room);
    if (name && room) {
      const { error, user } = Chat.addUser({ id: socket.id, name, room });

      if (error) return callback(error);

      socket.join(user.room);

      socket.emit('message', {
        user: 'admin',
        text: `${user.name}, welcome to room ${user.room}.`,
      });
      socket.broadcast
        .to(user.room)
        .emit('message', { user: 'admin', text: `${user.name} has joined!` });

      io.to(user.room).emit('roomData', { room: user.room, users: Chat.getUsersInRoom(user.room) });

      callback();
    }
  });

  socket.on('sendMessage', (message, callback) => {
    const user = Chat.getUser(socket.id);

    io.to(user.room).emit('message', { user: user.name, text: message });

    callback();
  });

  socket.on('disconnect', () => {
    const user = Chat.removeUser(socket.id);

    if (user) {
      io.to(user.room).emit('message', { user: 'Admin', text: `${user.name} has left.` });
      io.to(user.room).emit('roomData', { room: user.room, users: Chat.getUsersInRoom(user.room) });
    }
  });
});

const connection = models.dbConn;

connection()
  .then(async () => {
    server.listen(process.env.PORT || 8000, () =>
      console.log(`Listening on port ${process.env.PORT}`)
    );
  })
  .catch((error) => console.error(error));
