import Transaction from '../models/Transaction';

exports.findAllTransactions = () =>
  Transaction.find().then((transactions) => {
    console.log('All Transactions queried from  mongoDB');
    return transactions;
  });

exports.findById = async (idNum) => {
  const transaction = await Transaction.findOne({ id: idNum });
  return transaction;
};

exports.findAllBySender = (address) => Transaction.find({ from: address });

exports.findAllByRecipient = (address) => Transaction.find({ to: address });

exports.insertTransaction = (transaction) =>
  Transaction.create(transaction).then((insertedTransaction, err) => {
    if (insertedTransaction) {
      return insertedTransaction;
    }
  });

// eslint-disable-next-line max-len
exports.deleteTransaction = (conditions) =>
  Transaction.deleteOne(conditions).then((err, result) => {
    if (err) {
      console.error(err);
      return err;
    }
    return result;
  });
