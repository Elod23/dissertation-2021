import Stock from '../models/Stock';

exports.findAllStocks = () =>
  Stock.find().then((stocks) => {
    console.log('All Stocks queried from  mongoDB');
    return stocks;
  });

exports.findById = async (idNum) => {
  const stock = await Stock.findOne({ id: idNum });
  return stock;
};

exports.insertStock = (stock) =>
  Stock.create(stock).then((insertedTransaction, err) => {
    if (insertedTransaction) {
      return insertedTransaction;
    }
  });

// eslint-disable-next-line max-len
exports.deleteStock = (conditions) =>
  Stock.deleteOne(conditions).then((err, result) => {
    if (err) {
      console.error(err);
      return err;
    }
    return result;
  });

exports.findAllOrdesForTicker = (ticker) => {
  Stock.find({ ticker: ticker })
    .populate('orders')
    .then((err, res) => {
      if (!err) {
        return res;
      } else {
        throw err;
      }
    });
};
