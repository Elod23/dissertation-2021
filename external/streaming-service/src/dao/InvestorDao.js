import Investor from '../models/Investor';

// eslint-disable-next-line max-len
exports.authenticate = async (userObj) =>
  Investor.find({ username: userObj.username, password: userObj.password });

exports.findAllInvestors = () =>
  Investor.find().then((investors) => {
    console.log('All investors queried from  mongoDB');
    return investors;
  });

exports.findById = async (id) => {
  const investor = await Investor.findOne({ _id: id });
  return investor;
};

exports.findByUsername = async (username) => {
  let investor = await Investor.findOne({ username: username });
  return investor;
};

exports.findByAddress = async (address) => {
  let investor = await Investor.findOne({ address: address });
  return investor;
};

exports.findByEmail = (userEmail) =>
  Investor.findOne({ email: userEmail }).then((investor) => {
    return investor;
  });

exports.insertInvestor = (investor) =>
  Investor.create(investor).then((insertedInvestor, err) => {
    if (!err) {
      return investor;
    }
    if (!insertedInvestor) {
      return err;
    }
  });

exports.updateInvestor = (conditions, update, options) =>
  Investor.updateOne(conditions, update, options).then((investor) => {
    console.log(`Investors fulfilling conditon:${JSON.stringify(conditions)} have been updated`);
    return investor;
  });

exports.removeFromWatchList = async (conditions, ticker, options) => {
  const investor = await Investor.findOneAndUpdate(conditions, { $pull: { watchlist: ticker } });
  if (investor) {
    console.log(`${conditions.address} does not follow ${ticker} anymore!`);
  }
  return investor;
};

exports.addToWatchList = async (conditions, ticker) => {
  const inv = await Investor.findOne(conditions);
  if (!inv.watchlist.includes(ticker)) {
    const res = await Investor.findOneAndUpdate(conditions, { $push: { watchlist: ticker } });
    if (res) {
      console.log(`${res.address} does follow ${ticker} now!`);
    }
    return res;
  } else {
    return inv;
  }
};

exports.deleteInvestor = (conditions) =>
  Investor.deleteOne(conditions).then((err, result) => {
    if (err) {
      console.error(err);
      return err;
    }
    return result;
  });
