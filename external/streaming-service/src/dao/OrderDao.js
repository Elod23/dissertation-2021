import Order from '../models/Order';

exports.findAllOrders = () =>
  Order.find().then((orders) => {
    console.log('All orders queried from  mongoDB');
    return orders;
  });

exports.findAllUnfilledOrders = () =>
  Order.find({ filled: false }).then((orders) => {
    console.log('All available orders queried from  mongoDB');
    return orders;
  });

exports.findByObjectId = async (objId) => {
  const order = await Order.findOne({ _id: objId });
  return order;
};

exports.findByOrderId = async (orderId) => {
  const order = await Order.findOne({ orderId: orderId });
  return order;
};

exports.findByExternalOrderId = async (orderId) => {
  const order = await Order.findOne({ externalOrderId: orderId });
  return order;
};

exports.insertOrder = (order) =>
  Order.create(order).then((insertedOrder, err) => {
    if (!err) {
      return insertedOrder;
    }
    if (!insertedOrder) {
      return err;
    }
  });

exports.updateOrder = (conditions, update, options) =>
  Order.updateOne(conditions, update, options).then((order) => {
    console.log(`Orders fulfilling conditon:${JSON.stringify(conditions)} have been updated`);
    return order;
  });

exports.deleteOrder = (conditions) =>
  Order.deleteOne(conditions).then((err, result) => {
    if (err) {
      console.error(err);
      return err;
    }
    return result;
  });
