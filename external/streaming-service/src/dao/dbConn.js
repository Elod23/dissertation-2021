import mongoose from 'mongoose';

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
const connectDb = () =>
  mongoose.connect(
    process.env.DATABASE_URL ||
      'mongodb+srv://realtimeweb:LeBr0nl4kers@2021.m3v66.mongodb.net/2021?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true }
  );

module.exports = connectDb;
